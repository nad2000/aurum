"""

IMPORTANT!

This module handles all of the parsing logic for the formulas

Ie c > 1, so it be compared with an actual value

The basic structure is:

The market will give us values. Like C = 6

Then we go into our records which have things like C > 10.

We then replace C with it's value and check if this is true.

To do this replacing we need to parse the formula to find out where to put the value.

"""


from functools import update_wrapper
from string import split
import re
import itertools
import traceback
    
#try:
#    from interval import interval
#except ImportError as e:
#    os.system(r'pip.exe install pyinterval')
#    os.system(r'C:\Python27\Scripts\pip.exe install pyinterval')
#    os.system(r'C:\"Program Files"\Python2.7\Scripts\pip.exe install pyinterval')
#    from interval import interval

VERBOSE = False


def grammar(description, whitespace=r'\s*'):
    """Convert a description to a grammar.  Each line is a rule for a
    non-terminal symbol; it looks like this:
        Symbol =>  A1 A2 ... | B1 B2 ... | C1 C2 ...
    where the right-hand side is one or more alternatives, separated by
    the '|' sign.  Each alternative is a sequence of atoms, separated by
    spaces.  An atom is either a symbol on some left-hand side, or it is
    a regular expression that will be passed to re.match to match a token.

    Notation for *, +, or ? not allowed in a rule alternative (but ok
    within a token). Use '\' to continue long lines.  You must include spaces
    or tabs around '=>' and '|'. That's within the grammar description itself.
    The grammar that gets defined allows whitespace between tokens by default;
    specify '' as the second argument to grammar() to disallow this (or supply
    any regular expression to describe allowable whitespace between tokens)."""
    G = {' ': whitespace}
    description = description.replace('\t', ' ') # no tabs!
    for line in split(description, '\n'):
        lhs, rhs = split(line, ' => ', 1)
        alternatives = split(rhs, ' | ')
        G[lhs] = tuple(map(split, alternatives))
    return G

def decorator(d):
    def _d(fn):
        return update_wrapper(d(fn), fn)
    update_wrapper(_d, d)
    return _d

@decorator
def memo(f):
    cache = {}
    def _f(*args):
        try:
            return cache[args]
        except KeyError:
            cache[args] = result = f(*args)
            return result
        except TypeError:
            # some element of args can't be a dict key
            return f(args)
    return _f

def parse(start_symbol, text, grammar):
    """Example call: parse('Exp', '3*x + b', G).
    Returns a (tree, remainder) pair. If remainder is '', it parsed the whole
    string. Failure iff remainder is None. This is a deterministic PEG parser,
    so rule order (left-to-right) matters. Do 'E => T op E | T', putting the
    longest parse first; don't do 'E => T | T op E'
    Also, no left recursion allowed: don't do 'E => E op T'"""

    tokenizer = grammar[' '] + '(%s)'

    def parse_sequence(sequence, text):
        result = []
        for atom in sequence:
            tree, text = parse_atom(atom, text)
            if text is None: return Fail
            result.append(tree)
        return result, text

    @memo
    def parse_atom(atom, text):
        if atom in grammar:  # Non-Terminal: tuple of alternatives
            for alternative in grammar[atom]:
                tree, rem = parse_sequence(alternative, text)
                if rem is not None: return [atom]+tree, rem  
            return Fail
        else:  # Terminal: match characters against start of text
            m = re.match(tokenizer % atom, text)
            return Fail if (not m) else (m.group(1), text[m.end():])

    # Body of parse:
    return parse_atom(start_symbol, text)

Fail = (None, None)

MyLang = grammar("""expression => block
block => variable operator number | number operator variable operator number | variable operator variable | variable operator variable operator variable | number operator variable operator variable | variable operator variable operator number | number operator number operator variable | variable 
variable => [\w\d\;\s+-\/\(\)*]* 
operator => <=|>=|<>|!=|==|>|<|=
number => [+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?
logicalop => AND|OR""", whitespace='\s*')

##[\w\d\s+-\/\(\)*]* 


def parse_it(text):
    return parse('expression', text, MyLang)


def evaluate(x, definitions, fn=None):
    
    """

        This is the meat function -- the logic for comparing formulas is here!

    """
    try:
        l = parse_it(x)[0][1][1:]
    except Exception as e:
        if VERBOSE:
            print 'error:', e
        return True
    try:
        if len(l) == 1 and l[0][0] == 'variable':
            l.append(['operator', '=='])
            l.append(['number', 1])
        variables = [y[1].replace('(', '').replace(')', '') for y in l if y[0] == 'variable']
        for y in l:
            if y[0] == 'variable':
                y[1] = y[1].replace('(', '').replace(')', '')
            if y[0] == 'variable':
                y[1] = '{' + y[1] + '}'
            if y[0] == 'variable' and '%' in y[1]:
                y[1] = float(y[1].replace('%', ''))/100
            if y[0] == 'operator' and y[1] == '=':
                y[1] = '=='
        operators = [y[1] for y in l if y[0] == 'operator']
        numbers = [y[1] for y in l if y[0] == 'number']
    except Exception as e:
        #print e
        traceback.print_exc()
        return True

    try:
        if not all([v in definitions.keys() for v in variables]):
            if VERBOSE:
                print 'Some variables in this query are not defined, so returning true.'
                print 'Variables in query:'
                print variables
                print 'Defined Variables:'
                print definitions.keys()
            
            return True
        formula = ''.join([y[1] for y in l]) 
        if '==' in operators:
            n = float(numbers[0]) 
            v = variables[0]
            v1 = float(definitions.get(v)[0])
            v2 = float(definitions.get(v)[1])
            #print v1, '<=', n, '<=', v2
            result = bool(v1 <= n <= v2)
        elif '<>' in operators or '!=' in operators:
            n = float(numbers[0]) 
            v = variables[0]
            v1 = float(definitions.get(v)[0])
            v2 = float(definitions.get(v)[1])
            #print 'not ', v1, '<=', n, '<=', v2
            result = not bool(v1 <= n <= v2)
        else:
            c = itertools.combinations(''.join(['01' for y in variables]), len(variables))
            forms = []
            for cc in c:
                indexes = [int(i) for i in cc]
                defs = {}
                for v in variables:
                    defs[v] = definitions[v][indexes.pop()]
                formula_to_eval = formula.format(**defs).replace('inf', "float('inf')")
                forms.append(eval(formula_to_eval))
            result = any(forms)
        relevant_definitions = dict([(k,v) for k,v in definitions.items() if k in variables])
        if VERBOSE:
            print formula, relevant_definitions, result
        return result
    except Exception as e:
        if VERBOSE:
            print 'ERROR:', e
        return True


def evaluate_all(raw, definitions, fn=None):
    # we need to evaulate each formula in a record
    e = [evaluate(x, definitions, fn) for x in raw]
    if False in e:
        return False
    return True


if __name__ == '__main__':
    definitions = {
        'LY1': [2, 1],
        'ADX': [1, 1],
        'L': [1, 2], 
        'L/C': [1, 2], 
        'C': [2, 5],
        'LY1/B*C': [3, 4],
        'X': [8, 8]
    }
    res = ['ADX', '(LY1)/(B)*(C)>.5', 'L<=10' , '(L/C)>5', 'L>C']
    print evaluate_all(res, definitions)

        
