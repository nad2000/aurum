"""

This module handles pulling queries from excel


"""


from pprint import pprint 
import os.path
import json
import math
import time
import csv
import xlrd



def excel_ranges(file_path):
    wb = xlrd.open_workbook(file_path)
    sh = wb.sheet_by_index(0)
    vals = {}
    for row in xrange(1, 10000): 
        try:
            variable = sh.cell_value(row, 0).upper()
        except Exception as e: 
            variable = None
        try:
            current = float(sh.cell_value(row, 1))
        except Exception as e: 
            current = None
        try:
            high = float(sh.cell_value(row, 2))
        except Exception as e: 
            high = None
        try:
            low = float(sh.cell_value(row, 3))
        except Exception as e: 
            low = None
        if high is None and low is None:
            continue
        if high is None: 
            high = float('inf')
        if low is None:
            low = -float('inf')
        try:
            vals[variable] = sorted([low, high])
        except Exception as e: 
            pass
    print json.dumps(vals, indent=2)
    return vals
    

def get_from_excel(file_path):
    qs = []
    vals = excel_ranges(file_path)
    for k, v in vals.items():
        try:
            qs.append(translate_query(k, v))
        except Exception as e:
            pass
    qs = ','.join(qs)
    try:
        print json.dumps(vals, indent=4)
    except Exception as e:
        pass
    return qs


def translate_query(variable, q):
   variable = variable.upper()
   l = float(q[0])
   h = float(q[1])
   if l == h:
       gq = '{0}={1}'.format(variable, l)
   else:
       gq = '{0}<{1}<{2}'.format(l, variable, h)
   return gq


def get_query(file_path):
    qs = get_from_excel(file_path)
    return qs
    

if __name__ == "__main__":
    file_path = '/Users/francislarson/Downloads/Mkt Update for Variables-new.xlsx'
    print json.dumps(get_query(file_path), indent=4)
