"""
This is the main file of the application. It's a standard Flask application.

FLask is a python web framework. It's used by CERN.
Those who know Django can easily figure out flask.

In order to hire another developer to work within the whole application they will 
do fine if they know:

- python
- flask
- javascript
- MongoDB

Most of the application is left uncommented because any developer who has 
used a major webframework like Django or Flask will understand what's going on.

Generally: requests come in to the routes (these things: @app.route('/work'), etc)
and then we return things to the client, usually just stuff in the database.

The frontend is just templates and javascript / JQuery. Another developer would do well if 
they re-did the frontend in a framework like React or Angular.

The database is MongoDB but there is no reason you couldn't switch this to MySQL.

"""

import datetime
from functools import wraps
import random
from pprint import pprint
import json 
import string
import uuid
import datetime
from datetime import timedelta
import traceback
import logging
import os
import time
import sys
import threading
from string import ascii_lowercase

from flask import *
import jinja2
import jinja2.ext

import main
from main import db
import excel


# Checks if we are running as an executable (e.g. as en .EXE)
if getattr(sys, 'frozen', False):
    dir_ = os.path.dirname(sys.executable)
else:
    dir_ = os.path.dirname(os.path.realpath(__file__))

# tell where the js and html files are 
template_dir = os.path.join(dir_, 'templates')
static_dir = os.path.join(dir_, 'static')

# initialize the application 
app = Flask(__name__)
app.template_folder = template_dir
app.static_folder = static_dir


def check_perm(id):
    #checks if an is in the "permanent collection"
    if db['permanent'].find_one({"formula_number": id}) is not None:
        return True
    return False


def clean(r):
    # some cleaning of the record so we can return it nicely to the client
    if isinstance(r, list):
        for x in r: 
            try:
                try:
                    del x['_id']
                except Exception as e:
                    pass
                del x['formulas']
                
                try:
                    x['date'] = x['date'].strftime('%m/%d/%Y')
                except Exception as e:
                    x['date'] = datetime.datetime(1971, 1, 1).strftime('%m/%d/%Y')
                try:
                    x['permanent'] = check_perm(x['formula_number'])
                except Exception as e:
                    pass
            except KeyError as e:
                pass
        return r
    else:
        try:
            try:
                del x['_id']
            except Exception as e:
                pass
            del r['formulas']
            try:
                r['date'] = r['date'].strftime('%m/%d/%Y')
            except Exception as e:
                r['date'] = datetime.datetime(1971, 1, 1).strftime('%m/%d/%Y')
            try:
                r['permanent'] = check_perm(r['formula_number'])
            except Exception as e:
                print e
            return r
        except KeyError as e:
            print e
            return r


@app.route('/summary')
def summary():
   return render_template('summary.html')



def get_saved_paths(kind):
    # finds the paths where we've saved things like
    # the excel path, etc
    saved_paths = db['saved_paths'].find_one()
    if saved_paths is not None:
        saved_paths = saved_paths.get(kind)
    else:
        saved_paths = 'Not Set'
    return saved_paths


@app.route('/')
@app.route('/work')
def work_get():
    excel_path = get_saved_paths('excel')
    csv_export_path = get_saved_paths('csv_export')
    return render_template('working.html', excel=excel_path, csv_export_path=csv_export_path)


@app.route('/change_excel', methods=['GET', 'POST'])
def change_excel():
    if request.method == 'GET':
        saved_paths = get_saved_paths('excel')
        return render_template('excel_path.html', excel=saved_paths)
    else:
        path = request.form.get('excel').strip()
        p = db['saved_paths'].find_one()
        if p is None:
            db['saved_paths'].insert({'excel': path})
        else:
            db['saved_paths'].update({'_id': p['_id']}, {"$set":{'excel': path}})
        return redirect('/work')


@app.route('/api/records/<collection>/<formula_number>', methods=['DELETE'])
def api_delete_record(collection, formula_number):
    try: 
        formula_number = int(formula_number)
    except Exception as e:
        pass
    db[collection].remove({"formula_number": formula_number})
    return jsonify({"results": str(formula_number) + ' deleted'})


# do the query on the formula
@app.route('/api/records/<collection>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def api_query(collection):
    """

    This is the meat of the application

    We will call this when we want to filter results of the database.

    If there is a query, it's attached to the end of the incoming URL (parameters)

    Most of the logic for the queries are in main.py

    """
    if request.method == 'POST' and request.json.get('new_collection') is None:
        query = request.json.get('query')
        if query == 'excel':
            path = get_saved_paths('excel')
            query = excel.get_query(path)
        query = query.upper()
        print query
        current_working = request.json.get('currentWorking')
        excluded = [int(x) for x in request.json.get('excluded','').split(',') if x.isdigit()]
        outside = [int(x) for x in request.json.get('outside','').split(',') if x.isdigit()]
        merge_permanent = request.json.get('merge_permanent', 'false')
        merge_permanent_exclusion = request.json.get('merge_permanent_exclusion', 'false')
        count = 0
        # if we have requested "all", then we will run the query on all the collections
        if collection == 'all':
            r = []
            for col in db.collection_names():
                ls = ['permanent', 'permanent_snapshots', 'saved_paths', 'examples', 'working_files', 'permanent_exclusion_snapshots', 'permanent_exclusion']
                if col != 'system.indexes' and col not in ls:
                    count += db[col].count() 
                    try:
                        for x in main.full(col, query, excluded, outside, merge_permanent, merge_permanent_exclusion, current_working):
                            x['collection'] = col
                            r.append(x)
                    except KeyError as e:
                        print e 
                        pass
        else:
            r = main.full(collection, query, excluded, outside, merge_permanent, merge_permanent_exclusion, current_working)
            for x in r: x['collection'] = collection
        r = clean(r)
        res = {}
        res.update(request.json)
        r = sorted(r, key=lambda k: k['formula_number'])
        # Create the object we will return to the client
        nres = {
            'collection': collection, 
            'query': query, 
            'excluded': excluded,
            'outside': outside, 
            'merge_permanent': merge_permanent,
            'merge_permanent_exclusion': merge_permanent_exclusion,
            'results': r
        }
        res.update(nres)
        return jsonify(res)
    elif request.method == 'POST' and request.json.get('new_collection') is not None:
        new_col = request.json.get('new_collection','').strip()
        if new_col:
            collection = '_'.join(new_col.strip().lower().split())
        new_record = main.create_new_record(request.json, collection)
        return jsonify({'record': clean(new_record)})
    elif request.method == 'PUT':
        new_record = main.edit_record(request.json, collection)
        return jsonify({'record': clean(new_record)})
    elif request.method == 'DELETE':
        db[collection].drop()
        return jsonify({'results': []})


@app.route('/api/working/<name>')
def get_working(name):
    # get a saved working file
    r = db['working_files'].find_one({'name': name}, {'_id': 0})
    return json.dumps(r)


@app.route('/api/working/merge', methods=['POST'])
def merge_file():
    # do a merge of working files
    incoming = request.json
    incoming_results = request.json.get('results')
    current = db['working_files'].find_one({'name': 'current'}, {'_id': 0})
    if current is None:
        print 'No Current, so default to incoming...'
        current = incoming
    current_results = current['results']
    current_results_fn = [x['formula_number'] for x in current_results]
    for r in incoming_results:
        if r['formula_number'] not in current_results_fn:
            current_results.append(r)
    current_results = sorted(current_results, key=lambda k: k['formula_number'])

    ## factor excludes...
    excl = current.get('excluded', [])
    inc_excl = incoming.get('excluded', [])
    current_results = [x for x in current_results if x.get('formula_number') not in excl and x.get('formula_number') not in inc_excl]
    current['results'] = current_results
    already_merged = current.get('merged_files', [])
    if already_merged is None: 
        already_merged = []
    already_merged.append(incoming['name'] + ' @ ' + (datetime.datetime.utcnow() - timedelta(hours=8)).strftime('%Y-%m-%d %H:%M:%S'))
    current['merged_files'] = already_merged
    current['name'] = 'current'
    current['original_results'] = current_results
    db['working_files'].update({'name': 'current'}, {'$set': current}, upsert=True)
    return json.dumps(current)


@app.route('/api/working', methods=['GET', 'POST', 'DELETE'])
def api_working():
    """
        Function for actions relating to working files
    """
    if request.method == 'GET':
        # get all the files we've got
        r = list(db['working_files'].find({}, {'_id': 0, 'results': 0}).sort('_id', -1))
        return jsonify({'results': r})
    elif request.method == 'POST':
        # save a working file
        record = request.json
        print 'Trying to save new  working file...'
        print record.get('merge_permanent')
        print record.get('merge_permanent_exclusion')
        print record.keys()
        print 'length:', len(record.get('results')) 

        # removing the merged conditions so they don't 
        # pull from stuff later

        try:
            record['merge_permanent'] = False
            record['merge_permanent_exclusion'] = False
            record['original_results'] = record['results']
        except Exception as e:
            print e

        date = datetime.datetime.utcnow()
        d = date.strftime("%A")[0].lower()
        m = date.strftime('%m')
        hms = date.strftime('%H%M%S')
        day = date.strftime('%d')
        name = record.get('name', '').strip()
        if not name:
            name = d + m + day + '_'+ hms
        record.update({
            'date': date,
            'name': name,
        })
        db['working_files'].update({'name': name}, {'$set': record}, upsert=True)
        r = list(db['working_files'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify(record)
    elif request.method == 'DELETE':
        # delete a working file
        name = request.json['name']
        db['working_files'].remove({'name': name})
        r = list(db['working_files'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify({'results': r})


@app.route('/api/snapshots', methods=['GET', 'POST', 'DELETE'])
def api_snapshots():
    """
        Function for dealing with snapshots
    """
    if request.method == 'GET':
        # retrieve a snapshot
        r = list(db['permanent_snapshots'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify({'results': r})
    elif request.method == 'POST':
        # create a snappshot
        r = main.get_permanent()
        r = clean(r)
        date = datetime.datetime.utcnow()
        name = request.json.get('name', '')
        if not name:
            d = date.strftime("%A")[0].lower()
            m = date.strftime('%m')
            day = date.strftime('%d')
            hms = date.strftime('%H%M%S')
            name = d + m + day + '_' + hms
        doc = {
            'date': date,
            'name': name,
            'records': r, 
            'excluded': request.json.get('excluded', []),
        }
        db['permanent_snapshots'].update({'name': name}, doc, upsert=True)
        r = list(db['permanent_snapshots'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify({'results': r})
    elif request.method == 'DELETE':
        # delete a snapshot
        if request.json is not None:
            name = request.json.get('name')
        else:
            name = None
        current_ = 'current'
        if name is not None and name != current_:
            db['permanent_snapshots'].remove({'name': name})
        elif name != current_:
            db['permanent_snapshots'].drop()
        r = list(db['permanent_snapshots'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify({'results': r})
   

@app.route('/api/permanent_exclusion/snapshots', methods=['GET', 'POST', 'DELETE'])
def api_exclusion_snapshots():
    """
        Function for the exclusion snapshots. Same as for permanent.
    """
    if request.method == 'GET':
        r = list(db['permanent_exclusion_snapshots'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify({'results': r})
    elif request.method == 'POST':
        r = main.get_permanent_exclusion()
        r = clean(r)
        date = datetime.datetime.utcnow()
        name = request.json.get('name', '')
        if not name:
            d = date.strftime("%A")[0].lower()
            m = date.strftime('%m')
            day = date.strftime('%d')
            hms = date.strftime('%H%M%S')
            name = d + m + day + '_' + hms
        doc = {
            'date': date,
            'name': name,
            'records': r, 
            'excluded': request.json.get('excluded', []),
        }
        db['permanent_exclusion_snapshots'].update({'name': name}, doc, upsert=True)
        r = list(db['permanent_exclusion_snapshots'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify({'results': r})
    elif request.method == 'DELETE':
        if request.json is not None:
            name = request.json.get('name')
        else:
            name = None
        current_ = 'current'
        if name is not None and name != current_:
            db['permanent_exclusion_snapshots'].remove({'name': name})
        elif name != current_:
            db['permanent_exclusion_snapshots'].drop()
        r = list(db['permanent_exclusion_snapshots'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify({'results': r})


@app.route('/create')
def create_record():
    return render_template('create.html')


@app.route('/edit')
def edit_record():
    return render_template('edit.html')

@app.route('/delete')
def delete_collection():
    return render_template('delete.html')


@app.route('/upload')
def upload_collection():
    return render_template('upload.html')


@app.route('/api/cats')
def cats():
    # get the different categories (1, 2, 3, 4) of records
    r = ['all']
    for collection in db.collection_names():
        ls = db[collection].distinct('category')
        for y in ls:
            if y not in r:
                r.append(y.strip())
    return jsonify({'results': r})


@app.route('/api/summaries', methods=['GET', 'POST', 'DELETE'])
def api_gen_sum():
    # function for handling the summaries 
    if request.method == 'DELETE':
        name = request.json['name']
        db['summaries'].remove({'name': name})
        return jsonify({'results': 'removed ' + name})
    if request.method == 'POST':
        cat = request.json.get('category', "all")
        # just going to post it every time
        w = request.json.get('working_file')
        sum_name = request.json.get('name')
        r = []
        summary = {'Q':{}, 'N':{}, 'S':{}, 'R':{}, 'I':{}}
        typs = [0, 1, 2, 3, 4]
        if w is not None:
            wf_name = w.get('name')
            for mkt in summary.keys():
                for typ in typs:
                    res = []
                    if cat == 'all':
                        for x in w['results']: 
                            try: 
                                outcome = float(x[mkt][typ])
                                if (len(x[mkt]) > typ):
                                    res.append(outcome)
                            except Exception as e:
                                pass
                    else:
                        for x in w['results']: 
                            try: 
                                outcome = float(x[mkt][typ])
                                if (len(x[mkt]) > typ) and x['category'] == cat:
                                    res.append(outcome)
                            except Exception as e:
                                pass
                    if typ not in [2, 3]:
                        avg_res = round(float(sum(res))/len(res), 3)
                        summary[mkt][str(typ)] = avg_res 
                    else: 
                        summary[mkt][str(typ)] = round(float(sum(res))) 
            date = datetime.datetime.utcnow()
            d = date.strftime("%A")[0].lower()
            m = date.strftime('%m')
            day = date.strftime('%d')
            hms = date.strftime('%H%M%S')
            if sum_name is None or sum_name == '':
                sum_name = cat + '_' + day + '_' + wf_name + '_' + hms
            doc = {
                'date': date,
                'summary': summary,
                'name': sum_name, 
                'category': cat, 
                'working_file_name': wf_name
            }
            print json.dumps(doc, indent=4)
            if sum_name != 'no_save':
                db['summaries'].update({'name': sum_name}, {'$set': doc}, upsert=True)
                try: 
                    del doc['_id']
                except Exception as e:
                    pass
            return jsonify({'results': doc})
        else:
            r = []
            return jsonify({'results': r})
    if request.method == 'GET':
        r = list(db['summaries'].find({}, {'_id': 0}).sort('_id', -1))
        return jsonify({'results': r})
   

@app.route('/api/records')
def all_records():
    # returns all records
    m = {'all': []}
    ls = ['examples', 'saved_paths', 'working_files', 'summaries', 'permanent_snapshots', 'permanent_exclusion_snapshots']
    for collection in db.collection_names():
        if collection != 'system.indexes' and collection not in ls:
            try:
                m[collection] = clean(list(db[collection].find()))
            except KeyError as e:
                pass
    return jsonify({'results': m})


@app.route('/api/collections')
def get_collections():
    # for getting all the collections
    ls = ['examples', 'saved_paths', 'working_files', 
        'summaries', 'permanent_snapshots', 
        'permanent_exclusion_snapshots', 'system.indexes'
    ]
    if request.args.get('everything', 'false') == 'true':
        ls = []
    r = [x for x in db.collection_names() if x not in ls]
    return jsonify({'results': r})


@app.route('/api/queryable')
def get_queryable():
    # this is to return all the collections that can be queried 
    # Some can't because they are summaries, etc, --- things that h
    # have already been calculated, etc...
    ls = ['examples', 'saved_paths', 'working_files', 
        'summaries', 'permanent_snapshots', 
        'permanent_exclusion_snapshots', 'system.indexes'
    ]
    col = [x for x in db.collection_names() if x not in ls]
    w = db['working_files'].find()
    work = ['working_file:'+x.get('name', '') for x in w]
    col += work
    return jsonify({'results': col})
        

@app.route('/api/directory', methods=['POST', 'GET'])
def api_directory():
    if request.method == 'POST':
        directory = request.json.get('directory', '').strip()
        r = main.walk_directory(directory)
        return jsonify({"directory": directory})


@app.route('/api/permanent', methods=['POST', 'GET', 'DELETE'])
def api_permanent():
    # function for handling the permanent collection
    if request.method == 'GET':
        r = main.get_permanent() 
        r = clean(r)
        res = {'results': r}
        res['state'] = db['perm_state'].find_one({'name': 'current'}, {'_id': 0})
        return jsonify(res)
    if request.method == 'POST':
        formula_numbers = request.json.get('formula_numbers', '').split(',')
        formula_numbers = [int(y.strip()) for y in formula_numbers]
        if request.json.get('action') == 'add':
            for formula_number in formula_numbers:
                r = main.add_permanent(formula_number)
        elif request.json.get('action') == 'remove':
            for formula_number in formula_numbers:
                r = main.remove_permanent(formula_number)
        r = main.get_permanent() 
        r = clean(r)
        res = {'results': r}
        res['state'] = db['perm_state'].find_one({'name': 'current'}, {'_id': 0})
        return jsonify(res)
    if request.method == 'DELETE':
        db['permanent'].drop()
        res = {'results': []}
        return jsonify(res)


@app.route('/api/permanent_exclusion', methods=['POST', 'GET', 'DELETE'])
def api_permanent_exclusion():
    # function for handling exclusion collection
    if request.method == 'GET':
        r = main.get_permanent_exclusion() 
        r = clean(r)
        res = {'results': r}
        res['state'] = db['perm_ex_state'].find_one({'name': 'current'}, {'_id': 0})
        return jsonify(res)
    if request.method == 'POST':
        formula_numbers = request.json.get('formula_numbers', '').split(',')
        formula_numbers = [int(y.strip()) for y in formula_numbers]
        print request.json
        if request.json.get('action') == 'add':
            for formula_number in formula_numbers:
                r = main.add_permanent_exclusion(formula_number)
        elif request.json.get('action') == 'remove':
            for formula_number in formula_numbers:
                r = main.remove_permanent_exclusion(formula_number)
        r = main.get_permanent_exclusion() 
        res = {'results': r}
        res['state'] = db['perm_ex_state'].find_one({'name': 'current'}, {'_id': 0})
        return jsonify(res)
    if request.method == 'DELETE':
        db['permanent_exclusion'].drop()
        res = {'results': []}
        return jsonify(res)


# function for exporting a set of formula records to csv
@app.route('/api/export/working_file', methods=["POST"])
def api_export_working():
    records = request.json['records']
    directory = request.json['directory']
    path = main.export_csv(directory, records)
    return jsonify({'message': 'saved to '+ path})


# function for exporting a db
@app.route('/api/save_database', methods=['POST'])
def api_save_database():        
    options = request.json
    print options
    directory = request.json.get('directory').strip()
    main.save_database(directory, options=options)
    return jsonify({'message': directory})


# function for loading an exported db
@app.route('/api/load_database', methods=['POST'])
def api_load_database():        
    overwrite = bool(request.json.get('overwrite', False))
    overwrite_files = bool(request.json.get('overwrite_files', False))
    directory = request.json.get('directory').strip()
    options = request.json
    main.load_database(directory, options=options, drop_previous=overwrite, drop_previous_files=overwrite_files)
    return jsonify({'message': directory})


@app.route('/api/upload', methods=['POST'])
def import_objects():        
    print request
    file = request.files['file']
    print file
    print request
    save_path = 'files'
    fn = file.filename.split('.')[0]
    print 'fn:', fn 
    location = os.path.join(dir_, save_path, fn)
    print 'location:', location
    file.save(location)
    main.create_collection_from_file(location, fn) 
    return jsonify({'message': fn})


@app.route('/all')
def index():
    return render_template('index.html')


@app.route('/database')
def database_ops():
    # checks the paths to export / load the database
    p = db['saved_paths'].find_one()
    if p is not None:
        default_load = p.get('default_load', '')
        default_save = p.get('default_save', '')
    else:
        default_load = default_save = ''
    return render_template('database_ops.html', 
        default_load=default_load, default_save=default_save)


@app.route('/api/database', methods=['DELETE'])
def ddd_api():
    if request.method == 'DELETE':
        main.delete_database()
        return 'thanks'


@app.route('/permanent')
def perm_recs():
    return render_template('permanent.html')


@app.route('/permanent_exclusion')
def perm_exck():
    return render_template('permanent_exclusion.html')


@app.route('/past_queries')
def past_queries():
    return render_template('past_queries.html')


# special error handlers for Flask
@app.errorhandler(400)
@app.errorhandler(401)
@app.errorhandler(403)
@app.errorhandler(404)
@app.errorhandler(500)
def error(e, user=None):
    messages = {
        400: 'Bad Request - Possibly missing a required parameter.',
        401: 'Unauthorized - No valid API key provided.',
        402: 'Request Failed - Parameters were valid but request failed.',
        403: 'Forbidden', 
        404: 'Not Found - The requested item does not exist.',
        500: 'Internal Error - Something has gone wrong with our application',
    }
    error = traceback.format_exc()
    try:
        code = e.code
    except Exception:
        code = 500
    if code == 500:
        print request.path
        print error
    response = json.jsonify({'code': code,'message': messages[code]})
    return response


# this is for saving the state of the application --
# we could also save this in a cookie....
@app.route('/api/state', methods=['GET', 'POST'])
def save_state():
    if request.method == 'POST':
        data = request.json
        db['workingState'].update({'name':'global'}, {'$set': data}, upsert=True)
        return jsonify(data) 
    if request.method == 'GET':
        data = db['workingState'].find_one({'name':'global'}, {'_id': 0}) or {}
        return jsonify(data) 
    

if __name__ == "__main__":
    app.run(threaded=True)
    #app.run()
