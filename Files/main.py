"""

This module includes a lot of the functions called in app.py -

It's most of the heavy lifting, ie:

- queries
- parsing old-type files (look at parse_ functions and "bounds")
- saving, exporting databases
- permanent / exclusion operations
- adding new records into the database from the UI 
- editing records

On a very high level:

Requests go to app.py, then depending on what it is, 
the appropriate function will run in here, and return
something to the client.

"""

import random
import string
import sys
import os
import json
import re
import traceback
from pprint import pprint 
import datetime
import socket
import evaluate
import csv
from pymongo import MongoClient 
from pymongo.errors import ServerSelectionTimeoutError
import pytz
from pyparsing import *
try:
    import win32com
except Exception as e:
    if os.name == 'win' or os.name == 'nt':
        os.system('pip install pypiwin32')
        os.system('C:\Python27\Scripts\pip.exe install pypiwin32')
        try:
            import win32com
        except Exception as e:
            pass

if getattr(sys, 'frozen', False):
    dir_ = os.path.dirname(sys.executable)
    MONGO_PORT = 38128
else:
    dir_ = os.path.dirname(os.path.realpath(__file__))
    MONGO_PORT = 27017

def start_mongo():
    """
    Attempts to start MongoDB
    """
    if os.name == 'win' or os.name == 'nt':
        try:
            LOG_DIR = os.path.join(dir_, "log")
            LOG_PATH = os.path.join(LOG_DIR, "database.log")
            DB_DIR = os.path.join(dir_, "db")
            MONGOD = os.path.join(dir_, "mongodb", "bin", "mongod.exe")
            win32com.shell.shell.ShellExecuteEx(
                lpVerb='runas',
                lpFile="netsh",
                lpParameters=r'advfirewall firewall add rule name="Open Port Mongo" dir=in action=allow protocol=TCP localport=%d' % MONGO_PORT)
            #os.system(r'netsh advfirewall firewall add rule name="Open Port Mongo" dir=in action=allow protocol=TCP localport=%d' % MONGO_PORT)
            if not os.exists(DB_DIR):
                os.makedirs(DB_DIR)
            if not os.exists(LOG_DIR):
                os.makedirs(LOG_DIR)
            try:
                win32com.shell.shell.ShellExecuteEx(
                    lpVerb='runas',
                    lpFile=MONGOD,
                    lpParameters=(r'--port %d --logappend --dbpath '
                        '"%s" --logpath "%s" --journal --storageEngine=mmapv1') %
                        (MONGOD, MONGO_PORT, DB_DIR, LOG_PATH))
                #os.system(
                #r'start cmd /k "%s" --port %d --logappend --dbpath "%s" --logpath "%s" --journal --storageEngine=mmapv1' %
                #(MONGOD, MONGO_PORT, DB_DIR, LOG_PATH))
                print(
                r'start cmd /k "%s" --port %d --logappend --dbpath "%s" --logpath "%s" --journal --storageEngine=mmapv1' %
                (MONGOD, MONGO_PORT, DB_DIR, LOG_PATH))
            except Exception as e:
                for x in range(10):
                    try:
                        # 'C:\"Program Files"\MongoDB\Server\3.0\bin\mongod.exe'
                        os.system(
                        r'start cmd /k C:\"Program Files"\MongoDB\Server\3.%d\bin\mongod.exe --port %d --logappend --dbpath "%s" --logpath "%s"' %
                        (x, MONGO_PORT, DB_DIR, LOG_PATH) )
                    except Exception as e:
                        pass
                    else:
                        break
        except Exception as e:
            # fallback
            os.system(r"start cmd /k C:\MongoDB\bin\mongod.exe")


conn = MongoClient(host='127.0.0.1',
            port=MONGO_PORT, 
            connectTimeoutMS=1000, 
            serverSelectionTimeoutMS=1000)

# Probe MongoDB:
try:
    info = conn.server_info()
except ServerSelectionTimeoutError:
    # start sever if connection failed
    start_mongo()

db = conn.main_app
permanent = db['permanent']
permanent_exclusion = db['permanent_exclusion']


def pacific_time():
    date = datetime.datetime.now(tz=pytz.utc)
    date = date.astimezone(pytz.timezone('US/Pacific'))
    return date


def bounds(y):
    """
    Assuming var with be on the left:
 
       'CU': [ 0, 1000], # 0 < CU < 1000
       'B': [ -float("inf"), 5],# B < 5
       'X': [ -10, float("inf")], # X > -10

    but some of the docs are like really weird
    like this:
    
        SP_STOCKS>10D_MA<65

     We can integrate this:
   
        10D_MA < SP_STOCKS < 65

        SP_STOCKS : [10D_MA, 65]

    but it's not clear this what is conceptually desired...

    so for now just treating the middle variable as the one desired.

    ie:
   
        6 > F < 19 we care about F
    
        6 < F > 19 we care about F

    """
    s = y.split('<')

    if len(s) > 1 and (len(s[0].split('>')) > 1 or len(s[1].split('>')) > 1):

        gh = s[0].split('>')
        if len(gh) > 1:
            # the original line is like 6 > F < 19
            # this line there's opposite signs like ["6 > F", "19"]
            var = gh[1]
            f = [-float('Inf'), gh[0], s[1]]
        else:
            gh = s[1].split('>')
            if len(gh) > 1:
                # the original line is like 6 < F > 19
                # this line there's opposite signs like ["6", "F > 19"]
                # means that F > 6 and F > 19
                var = gh[0] 
                f = [s[0], gh[1], float('Inf')]
            else:
                f = []
        if f:
            for i, v in enumerate(f):
               try:
                   f[i] = float(v)
               except Exception as e:
                   pass
            dic = {var: f}
        else:
            dic = {}

    else:

        if len(s) == 1:
            s = y.split('>')
            if len(s) == 3:
                var = s[1]
                try:
                    f = [float(s[0]), float(s[2])]
                except Exception as e:
                    f = [s[0], s[2]]
            elif len(s) == 2:
                var = s[0]
                try:
                    f = [float(s[1]), float('inf')]
                except Exception as e:
                    f = [s[1], float('inf')]
            else:
                f = []
        else:
            if len(s) == 3:
                var = s[1]
                try: 
                    f = [float(s[0]), float(s[2])]
                except Exception as e:
                    f = [s[0], s[2]]
            elif len(s) == 2:
                var = s[0]
                try:
                    f = [-float('inf'), float(s[1])]
                except Exception as e:
                    f = [-float('inf'), s[1]]
            else:
                f = []
        if not f:
            s = y.split('=')
            if len(s) == 2:
                var = s[0]
                try:
                    f = [float(s[1]), float(s[1])] 
                except Exception as e: 
                    f = [s[1], s[1]] 
            else:
                f = []
        if f:
            dic = {var: f}
        else:
            dic = {}
    for k, v in dic.items():
               
        new_v = []
        if '.' in k:
            try:
                del dic[k]
            except Exception as e:
                pass
            k = k.replace('.', '@')
        for vv in v:
            if isinstance(vv, str) and '%' in vv:
                vv = vv.strip('%')
                try:
                    vv = float(vv)
                except Exception as e:
                    pass 
            if not isinstance(vv, str):
                new_v.append(vv)
        if len(new_v) == 1 and (new_v[0] == float('inf') or new_v[0] == -float('inf')):
            try:
                del dic[k]
            except Exception as e:
                pass
        else:
            dic[k] = new_v
    return dic


def parse_date(s):
    try:
        date = datetime.datetime.strptime(s, '%m/%d/%y')
    except Exception as e:
        try:
            date = datetime.datetime.strptime(s, '%m/%d/%Y')
        except Exception as e:
            try:
                date = datetime.datetime.strptime(s, '%-m/%-d/%y')
            except Exception as e:
                try:
                    date = datetime.datetime.strptime(s, '%-m/%-d/%y')
                except Exception as e:
                    print traceback.print_exc()
                    date = 'Improper Date: Must be format mm/dd/YY' 
    return date



def parse_bounds(y, formula_doc):
    dic = {}
    try:
        dic  = bounds(y)
    except Exception as e:
        print traceback.print_exc()
        dic = {y : ['Improper formula format: variables must be on left and formulas must be enclosed in parentheses']}
    if dic: 
        multiples = [i for i in dic.keys() if i in  formula_doc.keys()]
        if multiples:
            key = dic.keys()[0]
            new_vals = dic[key]
            ex_vals = formula_doc[key]
            merged = list(set(new_vals) ^ set(ex_vals))
            if len(merged) > 3:
                if sorted(merged)[0] == -float('Inf') and sorted(merged)[-1] == float('Inf'):
                    merged = sorted(merged)[1:-1]
            dic[key] = merged
            
    return dic


def parse_formulas(s):
    raw_formulas = s
    intervals = {}
    comments = []
    try:
        for x in nestedExpr(opener='{', closer='}').searchString(raw_formulas): 
            comment = "{" + x[0][0] + "}"
            raw_formulas = raw_formulas.replace(comment, "")
            comments.append(comment)
        
        if '[' in raw_formulas or ']' in raw_formulas:
            bs = re.findall(r"\[.*?\]", raw_formulas)
            for x in bs:
                raw_formulas = raw_formulas.replace(x, '')
            bs = [y.strip('[').strip(']') for y in bs if y]
        else:
            bs = []
        raw_formulas = [x.strip(')') for x in raw_formulas.split('(') if x]
        raw_formulas += bs
        raw_formulas = [x.replace('(', '').replace(')', '').replace(' ', '') for x in raw_formulas]
    except Exception as e:
        print 'Cannot Parse Certain Formula/Comment', e, '...Ignoring...'
    else:
        for y in raw_formulas:
            try:
                dic = parse_bounds(y, intervals)
                intervals.update(dic)
            except Exception as e:
                print traceback.print_exc()
                intervals.update({y: 'Improper Formula: Must be format: (CU16<3)(L>4)(H<120)(C0>2.7)'})
    return intervals, raw_formulas, comments


def parse_market(s):
    try: 
        cl = lambda yy: [round(float(xx),2) for xx in yy.split()]
        d = cl(s)
    except Exception as e:
        try: 
            cl = lambda yy: [xx for xx in yy.split()]
            d = cl(s)
        except Exception as e:
            print traceback.print_exc()
            ff = 'Improper mkt data: must be formatted: 81.000000 0.080000 7.000000 4.000000 0.560000'
            d = ff 
    return d


def parse_category(s):
    try:
        signal = s.split(',')[0]
    except Exception as e:
        print traceback.print_exc()
        signal = "Improper Category format: must be on line 2"
    return signal


def parse_comments(s):
    try: 
        extra = re.search(r'{(.*)}', s)
        if extra is not None:
            extra = extra.group()
            extra = [yy.strip('{') for yy in extra.split('}')]
            extra = ['{{{0}}}'.format(yy) for yy in extra if yy]
        else: 
            extra = []
    except Exception as e:
        print traceback.print_exc()
        extra = 'Improper comments format: must be enclosed by { }'
    return extra


def parse_formula_number(s):
    try:
        formula_number = int([y.strip('.') for y in s.split(',')[1:]][0])
    except Exception as e:
        print traceback.print_exc()
        formula_number = 'Improper formula number' 
    return formula_number


def parse_string(raw_s):
    parsed = []
    raw = raw_s.split('#') 
    for y in raw: 
        if y:
            x = y.splitlines() 
            x = [yy for yy in x if yy]
            if len(x) != 8:
                x = y.split('\r\n')
                x = [yy for yy in x if yy]
                if len(x) != 8:
                    x = y.split('\n')
                    x = [yy for yy in x if yy]
                    if len(x) != 8:
                        x = y.split('\r')
                        x = [yy for yy in x if yy]
            x = [yy.strip() for yy in x]
            x = [yy for yy in x if yy]
            try:
                date = parse_date(x[0])
            except IndexError as e:
                date = 'missing date'
            try:
                category = parse_category(x[1])
            except IndexError as e:
                category = 'missing category'
            try:
                formula_number = parse_formula_number(x[1])
            except IndexError as e:
                formula_number = 'missing formula number'
            try:
                intervals, raw_formulas, comments = parse_formulas(x[2])
            except IndexError as e:
                intervals, raw_formulas, comments = 'missing formulas', 'missing formulas, missing comments'
            try:
                d1 = parse_market(x[3]) 
            except IndexError as e:
                d1 = 'no data'
            try:
                d2 = parse_market(x[4]) 
            except IndexError as e:
                d2 = 'no data'
            try:
                d3 = parse_market(x[5]) 
            except IndexError as e:
                d3 = 'no data'
            try:
                d4 = parse_market(x[6]) 
            except IndexError as e:
                d4 = 'no data'
            try:
                d5 = parse_market(x[7]) 
            except IndexError as e:
                d5 = 'no data'
            doc = {
                'raw_record': x,
                'date': date,
                'formulas': intervals,  
                'raw_formulas': raw_formulas,
                'Q': d1,
                'N': d2,
                'S': d3,
                'R': d4,
                'I': d5,
                'category': category,
                'formula_number': formula_number,
                'comments': comments,
            }
            parsed.append(doc)
    return parsed


def parse(fn):
    with open(fn, 'rb') as f:
        raw = f.read()
        parsed = parse_string(raw)
    return parsed



def export_csv(directory, records):
    """
        function for exporting the working file numbers to csv 
        in a particular directory. Similar to save_database
    """
    # WF_YYYY-MM-DD.csv
    date = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    if not os.path.exists(directory):
        os.mkdir(directory)
    filename = 'WF_{0}.csv'.format(date)
    path = os.path.join(directory, filename)
    with open(path, 'wb') as f:
        c = csv.writer(f)
        c.writerow(records)  
    
    p = db['saved_paths'].find_one()
    if p is None:
        db['saved_paths'].insert({'csv_export': directory})
    else:
        db['saved_paths'].update({'_id': p['_id']}, 
            {"$set":{'csv_export': directory}}, upsert=True)
    return path



def save_database(directory, options):
    """
        Function for exporting a database
    """
    date = datetime.datetime.now().strftime('%m-%d-%Y-%H-%M')
    if not os.path.exists(directory):
        os.mkdir(directory)
    new_directory = directory
    if not os.path.exists(new_directory):
        os.mkdir(new_directory)
    p = db['saved_paths'].find_one()
    if p is None:
        db['saved_paths'].insert({'default_save': new_directory})
    else:
        db['saved_paths'].update({'_id': p['_id']}, {"$set":{'default_save': new_directory}})
    cs = []
    ac = db.collection_names()
    if options.get('all') == 'on':
        cs = ac
    else:
        if options.get('collections') == 'on':
            ccs = [x for x in ac if db[x].find_one() and len(db[x].distinct('raw_formulas')) > 0]
            perms = ['permanent', 'permanent_exclusion']
            ccs = [x for x in ccs if x not in perms]
            cs.extend(ccs)
        if options.get('perm') == 'on': 
            cs.extend(['permanent', 'permanent_snapshots'])
        if options.get('permex') == 'on': 
            cs.extend(['permanent_exclusion', 'permanent_exclusion_snapshots'])
        if options.get('working') == 'on': 
            cs.extend(['working_files'])
        if options.get('sum') == 'on': 
            cs.extend(['summaries'])
        if options.get('paths') == 'on': 
            cs.extend(['saved_paths'])

    for col in cs:
        if db[col].find_one() and len(db[col].distinct('raw_formulas')) > 0 or col == 'permanent' or col == 'permanent_exclusion':
            fn = '__collection__' + col
        else:
            fn = col
        save_filename = os.path.join(new_directory, fn)
        with open(save_filename, 'wb') as f:
            bl = [x for x in db[col].find({}, {'_id': 0})]
            for x in bl:
                for k, v in x.items():
                    if isinstance(v, datetime.date):
                        try:
                            v = v.isoformat()
                            x[k] = v
                        except Exception as e:
                            pass
                if x.get('date') is not None:
                    try:
                        x['date'] = x['date'].isoformat() 
                    except Exception as e:
                        pass
            f.write(json.dumps(bl, indent=4))


def delete_database():
    conn.drop_database('main_app')
    #for col in db.collection_names():
    #     try:
    #         db.drop_collection(col)
    #     except Exception as e:
    #         pass
            

def load_database(directory, options, drop_previous=True, drop_previous_files=True):
    """
        Function for loading exported databases
    """
    fs = [] 
    if os.path.exists(directory):
        for root, dirs, files in os.walk(directory):
            for file in files:
                p = os.path.join(root,file)
                fs.append(os.path.abspath(p))

    everything = False
    collections = False
    cs = []
    if options.get('all') == 'on':
        everything = True
    else:
        if options.get('collections') == 'on':
            collections = True
        if options.get('perm') == 'on': 
            cs.extend(['permanent', 'permanent_snapshots'])
        if options.get('permex') == 'on': 
            cs.extend(['permanent_exclusion', 'permanent_exclusion_snapshots'])
        if options.get('working') == 'on': 
            cs.extend(['working_files'])
        if options.get('sum') == 'on': 
            cs.extend(['summaries'])
        if options.get('paths') == 'on': 
            cs.extend(['saved_paths'])

    for filename in fs:
        print filename
        try:
            colname = os.path.basename(filename)
            if everything or (collections and '__collection__' in colname) or colname in cs or ('__collection__' in colname and colname.split('__')[2] in cs):
                if ('__collection__' in colname or 'perm' in colname) and 'snapshot' not in colname:
                    coltype = 'collection'
                elif 'working' in colname or 'snapshot' in colname:
                    coltype = 'file'
                else:
                    coltype = 'other'
                if '__collection__' in colname:
                    colname = colname.split('__')[2]
                f = filename
                print colname, cs, coltype
                with open(f, 'rb') as f:
                    try:
                        docs = json.loads(f.read())
                    except Exception as e:
                        print e
                        docs = []

                    if drop_previous and coltype != 'file':
                        print 'droping previous collection...'
                        try:
                            db[colname].drop()
                        except Exception as e:
                            print e 

                    if drop_previous_files and coltype == 'file':
                        print 'droping previous snapshot or working file...' 
                        try:
                            db[colname].drop()
                        except Exception as e:
                            print e 

                    print 'Filename:', filename 
                    print 'length of docs:', colname, len(docs)
                    print 'Collection Type:', coltype
                    for x in docs:
                        try:
                            try:
                                if x.get('date') is not None:
                                    x['date'] = datetime.datetime.strptime(x['date'], "%Y-%m-%dT%H:%M:%S")
                            except Exception as e:
                                pass
                            try:
                                del x['_id']
                            except Exception as e:
                                pass
                        except Exception as e:
                            print e

                        # it needs to do an updat e
                        if coltype == 'collection':
                            try:
                                db[colname].update({'formula_number': x['formula_number']}, {'$set': x}, upsert=True)
                            except Exception as e:
                                print e
                        elif coltype == 'file':
                            try:
                                print 'Updating file...'
                                db[colname].update({'name': x['name']}, {'$set': x}, upsert=True)
                            except Exception as e:
                                print e
                        else:
                            # drop first because singlular clllection
                            try:
                                db[colname].drop()
                            except Exception as e:
                                print e
                            try:
                                db[colname].insert(x)
                            except Exception as e:
                                print traceback.print_exc()
        except Exception as e:
            print e
    p = db['saved_paths'].find_one()
    print p
    if p is None:
        db['saved_paths'].insert({'default_load': directory})
    else:
        db['saved_paths'].update({'_id': p['_id']}, {"$set":{'default_load': directory}})
    print db['saved_paths'].find_one()




def main():
    directory = raw_input('please enter the directory where the files are: ')
    print 'Inserting docs into database...'
    walk_directory(directory)


def walk_directory(directory):
    print 'About to find records in files...dropping items in the records DB'
    for root, directories, filenames in os.walk(directory):
        print 'Walking...', directories 
        for filename in filenames: 
            print 'found file...', filename
            f = os.path.join(root, filename)
            full_path = os.path.dirname(os.path.abspath(f))
            full_filename = os.path.abspath(f)
            create_collection_from_file(f, filename)


def create_collection_from_file(f, filename):
    """
        This function is the main entry point for parse an old-type file
        into a new file for the database
    """
    docs = parse(f)
    if filename[0].isdigit(): filename = 'file' + filename 
    ###### REMEMBER THIS #######
    db[filename].remove({})
    ############################
    for x in docs:
        try:
            db[filename].insert(x)        
        except Exception as e:
            print e


def query(collection, current_working, **kwargs):

    """
        Function for handling queries -- relies on the evaluate.py

        This is the major heavy lifing of app.py
    """
    q = {}

    outside = kwargs.get('outside')
    if 'working_file' in collection:
        print 'Request for working file query'
        wf_name = collection.split(':')[1]
        if current_working is not None:
            print 'Using the sent working file...'
            r_to_q = current_working['results']
            if not r_to_q:
                print 'None found so looking for saved...'
                r_to_q =  db['working_files'].find_one({'name': wf_name})['results']
        else:
            print 'None found so looking for saved...'
            r_to_q =  db['working_files'].find_one({'name': wf_name})['results']
        records_to_eval = r_to_q
    else:
        records_to_eval = db[collection].find(q)
 
    perms = [x["formula_number"] for x in get_permanent()]
    final = [x for x in records_to_eval if evaluate.evaluate_all(x['raw_formulas'], kwargs, x['formula_number']) or x['formula_number'] in perms]
    if outside is not None:
        for collection in db.collection_names():
            if collection != 'system.indexes':
                for x in outside:
                    ress = db[collection].find_one({'formula_number': int(x)})
                    if ress is not None and x not in [y['formula_number'] for y in final]:
                        final.append(ress)
    return final


def get_next_record_number():
    m = []
    for collection in db.collection_names():
        if collection != 'system.indexes':
            m.extend(db[collection].distinct('formula_number'))
    m = sorted(m, reverse=True)
    next_number = m[0] + 1
    return next_number


def create_new_record(new_record, collection):
    new_record['date'] = datetime.datetime.now()
    new_record['formula_number'] = int(new_record.get('formula_number')) #or get_next_record_number() 
    new_record['comments'] = [y.strip().strip('{').strip('}').strip(',').strip('}') for y in new_record['comments'].split('{')]
    new_record['comments'] = [y for y in new_record['comments'] if y]
    new_record['comments'] = [('{' + yy) + '}'  for yy in new_record['comments']]
    new_record['raw_formula_numbers'] = [new_record['formula_number']]  # might need to change 
    new_record['raw_formula_numbers'] = [new_record['formula_number']]  # might need to change 
    ls = ['S', 'R', 'I', 'Q', 'N']
    for x in ls:
        new_record[x] = [round(float(y.strip()),2) for y in new_record[x].split(',') if y.strip()]
    new_record['raw_formulas'] = new_record['formulas'].split(',')
    new_record['formulas'] = clean(new_record['formulas'])
    db[collection].insert(new_record)
    return new_record


def edit_record(new_record, collection):
    new_record['date'] = datetime.datetime.now()
    new_record['formula_number'] = int(new_record.get('formula_number')) #or get_next_record_number() 
    new_record['comments'] = [y.strip().strip('{').strip('}').strip(',').strip('}') for y in new_record['comments'].split('{')]
    new_record['comments'] = [y for y in new_record['comments'] if y]
    new_record['comments'] = [('{' + yy) + '}'  for yy in new_record['comments']]
    new_record['raw_formula_numbers'] = [new_record['formula_number']]  # might need to change 
    ls = ['S', 'R', 'I', 'Q', 'N']
    for x in ls:
        new_record[x] = [round(float(y.strip()),2) for y in new_record[x].split(',') if y.strip()]
    new_record['raw_formulas'] = new_record['formulas'].split(',')
    new_record['formulas'] = clean(new_record['formulas'])
    pprint(new_record)
    db[collection].update({"formula_number": new_record["formula_number"]}, {"$set": new_record}, upsert=True)
    pprint(db[collection].find_one({'formula_number': new_record['formula_number']}))
    return new_record

    
def add_permanent(formula_number, collection=None):
    db['perm_state'].update({'name': 'current'}, {'$set': {'updated': datetime.datetime.now()}}, upsert=True)
    formula_number = int(formula_number)
    if collection is None:
        for collection in db.collection_names():
            ex = db[collection].find_one({'formula_number': formula_number})
            if ex is not None: break
    else:
        ex = db[collection].find_one({'formula_number': formula_number})
    print 'records found:', ex
    if ex is not None:
        del ex['_id']
        return permanent.update({'formula_number': formula_number}, {'$set': ex}, upsert=True)


def add_permanent_exclusion(formula_number, collection=None):
    db['perm_ex_state'].update({'name': 'current'}, {'$set': {'updated': datetime.datetime.now()}}, upsert=True)
    formula_number = int(formula_number)
    if collection is None:
        for collection in db.collection_names():
            ex = db[collection].find_one({'formula_number': formula_number})
            if ex is not None: break
    else:
        ex = db[collection].find_one({'formula_number': formula_number})
    print 'records found:', ex
    if ex is not None:
        del ex['_id']
        return permanent_exclusion.update({'formula_number': formula_number}, {'$set': ex}, upsert=True)


def remove_permanent_exclusion(formula_number):
    db['perm_ex_state'].update({'name': 'current'}, {'$set': {'updated': datetime.datetime.now()}}, upsert=True)
    print 'trying to remove', formula_number
    print 'perm count', permanent_exclusion.count()
    ex = permanent_exclusion.remove({'formula_number': formula_number})
    print 'new perm count', permanent_exclusion.count()
    return ex


def get_permanent_exclusion():
    r = list(permanent_exclusion.find({}, {'_id': 0}))
    return r


def get_permanent():
    r = list(permanent.find({}, {'_id': 0}))
    return r


def remove_permanent(formula_number):
    db['perm_state'].update({'name': 'current'}, {'$set': {'updated': datetime.datetime.now()}}, upsert=True)
    print 'trying to remove', formula_number
    print 'perm count', permanent.count()
    ex = permanent.remove({'formula_number': formula_number})
    print 'new perm count', permanent.count()
    return ex


def show_perm():
    show = raw_input('Show permanent record? (y/n):  ')
    if show == 'y':
        r = list(permanent.find())
        for i, x in enumerate(r, 1):
            print '\n\n'
            print '======================================='
            print 'Result Number: ', i
            print '\n\n'
            pprint(x)
            print '======================================='
            print '\n\n'
        print 'Number of Results: ', permanent.count()


def show_results(res):
    for i, x in enumerate(res, 1):
        print '\n\n'
        print '======================================='
        print 'Result Number: ', i
        print '\n\n'
        pprint(x)
        print '======================================='
        print '\n\n'
    print 'Number of results: ', len(res)


def show_add():
    add = raw_input('Add records to permanent record using the formula_number, separated by commas:  ')
    add = add.split(',')
    print 'Adding...', add
    for x in add:
        x = x.strip()
        add_permanent(x)

def show_remove():
    remove = raw_input('Remove records from the permanent record using formula_number, separated by commas:  ')
    remove = remove.split(',')
    print 'Removing...', remove
    for x in remove:
        x = x.strip()
        remove_permanent(x)


def clean(qs):
    qs = qs.split(',')
    q = {}
    for x in qs:
        x = x.replace(" ", "")
        q.update(bounds(x)) 
    return q


def full(collection, qs, excluded=None, outside=None, merge_permanent=None, merge_permanent_exclusion=None, current_working=None):
    q = clean(qs)
    if excluded is not None:
        q.update({'excluded': excluded}) 
    if outside is not None:
        q.update({'outside': outside})
    if merge_permanent is not None:
        q.update({'merge_permanent': merge_permanent})
    if merge_permanent_exclusion is not None:
        q.update({'merge_permanent_exclusion': merge_permanent_exclusion})
    return query(collection, current_working, **q)


def get_diff(a, b):
    b = set(b)
    return [aa for aa in a if aa not in b]


def diff(res, q): 
    new = open(dir_ + '/debug/new.txt', 'wb')
    for x in res:
        id = x.get('formula_number')
        if id is not None:
            new.write(str(id) + '\n')
    new.close()
    
    old = sorted([x.strip() for x in open(dir_ + '/debug/old.txt', 'rb').read().splitlines()])
    new = sorted([x.strip() for x in open(dir_ + '/debug/new.txt', 'rb').read().splitlines()])
    in_old_not_new = get_diff(old, new)
    in_new_not_old = get_diff(new, old)
    o = open(dir_ + '/debug/diff.txt', 'wb')
    o.write('query\n')
    o.write(json.dumps(q))
    o.write('\nold' + str(len(old)) +' documents found\n')
    o.write(json.dumps(old))
    o.write('\nnew' + str(len(new)) +' documents found\n')
    o.write(json.dumps(new))
    o.write('\nIn old not new\n')
    o.write(json.dumps(in_old_not_new))
    o.write('\nIn new not old\n')
    o.write(json.dumps(in_new_not_old))
    o.close()


if __name__ == "__main__":
    import sys
    #x = bounds(sys.argv[1])
    #print x
    #collection = 'file1'
    #print query(collection, **x)
    main()
    #save_database('ghetto_sauce')
    #load_database('ghetto_sauce')
    #print get_next_record_number()
    #res = show_query()
    #show_results(res)
    #show_add()
    #show_perm()
    #show_remove()
    #show_perm()
    
      



