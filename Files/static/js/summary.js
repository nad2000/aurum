// this file is for the summaries on the working file page 


var allSummaries = [];
var currentSummary;

function getSummary(){
}

// show available summaries to view
function showSummaries(data) {
	var ul = document.getElementById("summaries");
            var li_str = "";
    var c = 0;
	data.forEach( function(d) {
        if (c == 0){
            var active = 'active';
        } else {
            var active = '';
        }
			li_str += '<li class="summaryItems '+active+'" value="' + d.name + '"><a class="summaryItem" id="'+d.name+'">' + d.name + '</a></li>';
        c++;
	});
	ul.innerHTML = li_str;
}

// display current summary
function showCurrentSummary(data) {
    currentSummary = data;
    if (data){
        var sumColl = '';
	    var sumTitle = "Summary: " + data.name;
        if (sumTitle.toLowerCase().indexOf(currentWorking.collection.toLowerCase()) > -1){
            sumColl = currentWorking.collection; 
	        $('#sumColl_label').show();
        } else {
	        $('#sumColl_label').hide();
        }
	    var sumDate = moment.tz(data['date'], "America/Los_Angeles");
	    var sumCat = data['category'];
        var pulledFrom = data.working_file_name || '';
	    document.getElementById('sumTitle').innerHTML = sumTitle;
	    document.getElementById('sumColl').innerHTML = sumColl;
	    document.getElementById('sumDate').innerHTML = sumDate;
	    document.getElementById('sumCat').innerHTML = sumCat;
	    document.getElementById('pulledFrom').innerHTML = pulledFrom;

	    var table = document.getElementById("summary");
        var li_str = "";
        //1.       Q
        //
        //2.       N
        //
        //3.       S
        //
        //4.       R
        //
        //5.       I

        //2.       The second column should be labeled “Avg. P/L”.
        //
        //3.       The third column should be labeled “Avg. Risk”.
        //
        //4.       The fourth column should be labeled “T. Wins”.
        //
        //5.       The fifth column should be labeled “T. Losses”.
        //
        //6.       The sixth column should be labeled “Avg. Return”.

        var headers = '<tr><th></th><th class="bluebold largefont">% Wins</th><th>Avg. P/L</th><th>Avg. Risk</th><th>T. Wins</th><th>T. Losses</th><th>Avg. Return</th></tr>';
        li_str += headers;
        
        function getWins(a, b){
            try {
                return a/(a + b)
            } catch(e){
                return 0;
            }
        }

        var correctOrder = ['Q', 'N', 'S', 'R', 'I'];
        var aavg = [];
        var rows = [];
	    Object.keys(data.summary).forEach( function(d) {
		    	var row = '<tr><td>'+ d +'</td><td class="bluebold largefont">'+ (getWins(data.summary[d][2], data.summary[d][3]).toFixed(3) * 100).toFixed(2) + ' %</td><td>'+data.summary[d][0]+'</td><td>'+data.summary[d][1]+'</td><td>'+data.summary[d][2]+'</td><td>'+data.summary[d][3]+'</td><td>'+data.summary[d][4]+'</td></tr>';
            rows.push([d, row]);
            aavg.push(getWins(data.summary[d][2], data.summary[d][3]));
    
	    });

        correctOrder.forEach(function(x){
            var r = rows.filter(function(d){
                return d[0] == x})[0][1];
            li_str += r;
        });            

        var tavgs;
        if (aavg.length > 0){
            tavgs = aavg.reduce(function(a, b) {return a + b;})/aavg.length;
        }else {
            tavgs = 0;
        }
        var tstring = '<tr><td></td><td class="bluebold largefont">'+ (tavgs.toFixed(3) * 100).toFixed(2) +'%</td>';
        for (var i=0; i < 5; i++){
            tstring += '<td></td>';
        }
        tavgs = '</tr>'
        li_str += tstring;
        li_str += tavgs; 

	    table.innerHTML = li_str;
    }
}

// get the available summaries
function summary(){
	$.ajax({
	   url: '/api/summaries',
	   type: 'get',
	   complete: function(d){
		d = JSON.parse(d.responseText)['results'];
		allSummaries = d; 
		showSummaries(d);
		showCurrentSummary(allSummaries[0]);
	   },
      });
}

// handle generate new one
$("#submit_generate_summary").click(function(){
    var name =  $.trim($('#summary_name').val());
    generateSummary(name);
    $('#submit_generate_summary').blur()
});

// generate but dont save anything
$("#submit_generate_summary_only").click(function(){
    var name = 'no_save';
    generateSummary(name);
    $('#submit_generate_summary_only').blur()
});

// delete a summary
$("#submit_delete_summary").click(function(){
    if (confirm('Are you sure you want to delete ' + currentSummary.name + '?')){
	    $.ajax({
	       url: '/api/summaries',
	       type: 'DELETE',
	       dataType: 'json', 
	       contentType: 'application/json', 
	       data: JSON.stringify(currentSummary), 
	       complete: function(d){
                summary();
	       },
          });
    }
    $('#submit_delete_summary').blur()
});

// handle clicking one of the available summaries
$(document).on("click", '.summaryItem', function(){ 
        var s = this.id;
		    $('.summaryItems').removeClass('active');
 	    $(this).parent().addClass('active');
		showCurrentSummary(allSummaries.filter(function(d){
            return d.name == s})[0]
        );
    
});


// gnerarate a summary by calling the application app.py
function generateSummary(name){
	var cat = $.trim($("#category option:selected" ).val());
	$.ajax({
	   url: "/api/summaries",
	   type: 'POST',
	   dataType: 'json', 
	   contentType: 'application/json', 
	   data: JSON.stringify({'category': cat, 'working_file': currentWorking, 'name': name}), 
	   complete: function(d){
		d = JSON.parse(d.responseText)['results'];
		showCurrentSummary(d);
        if (name != 'no_save'){
		    summary();
        }else {
		        $('.summaryItems').removeClass('active');
        }
	   },
	});
   }

// get available categories
function cats(){
	$.ajax({
	   url: "/api/cats",
	   type: 'GET',
	   complete: function(d){
		d = JSON.parse(d.responseText)['results'];
		showCats(d);
	   },
	});
	
}

// show the categories to be selected
function showCats(data) {
	var sel = document.getElementById("category");
    var options_str = "";
    //1.       “Category 1 – LB”
    //
    //2.       “Category 2 – Buy”
    //
    //3.       “Category 3 – Sell”
    //
    //4.       “Category 4 – Volatility”
    //

    var labels = {
        1: "LB",    
        2: 'Buy',
        3: 'Sell',
        4: 'Volatility',
    }
	var asd =['all', 1,2,3,4];
    asd.forEach( function(d) {
        var label = labels[d] || '';
			options_str += '<option value="' + d + '">Category ' + d + ' ' + label  + '</option>';
	});
	sel.innerHTML = options_str;
}

// call everything on first load
summary();
cats();



