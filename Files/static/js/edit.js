// file for editing records

var records = {};
var currentCollection = '';


// shows available collections
function show_collections(data) {
	var sel = document.getElementById("collection");
            var options_str = "";
	Object.keys(data).forEach( function(d) {
		if (d != 'all'){
				options_str += '<option value="' + d + '">' + d + '</option>';
		}
	});
	sel.innerHTML = options_str;
}

// shows records in selected ccollection
function show_records(data, id) {
	var sel = document.getElementById("formula_number");
            var options_str = "";
	data[id].sort(function(a, b){
		var keyA = a.formula_number,
	 	  keyB = b.formula_number;
		  // Compare the 2 dates
		if(keyA < keyB) return -1;
		if(keyA > keyB) return 1;
		return 0;
	});


		options_str += '<option value="" disabled selected>Select the formula number</option>';
	data[id].forEach( function(d) {
			options_str += '<option value="' + d.formula_number + '">' + d.formula_number + '</option>';
	});
	sel.innerHTML = options_str;
}


// displays record so we can edit
function show_record_to_edit(data, id, fn) {
	var ls = [];
  	data[id].forEach(function(d){
		if (d.formula_number == fn){
			ls.push(d)
		}
	});	
	var r = ls[0];
	document.getElementById("formulas").value = r.raw_formulas;
	document.getElementById("Q").value = r.Q;
	document.getElementById("N").value = r.N;
	document.getElementById("S").value = r.S;
	document.getElementById("R").value = r.R;
	document.getElementById("I").value = r.I;
	document.getElementById("category").value = r.category;
	document.getElementById("comments").value = r.comments;
	document.getElementById("currentFormulaNumber").innerHTML = r.formula_number;
}

    function get_records(){
	$.ajax({
	   url: '/api/records',
	   type: 'get',
	   complete: function(d){
		d = JSON.parse(d.responseText)['results'];
		currentCollection = Object.keys(d)[0];
		show_collections(d);
		records = d;
		show_records(d, currentCollection);
	   },
      });
}

// do stuff when we change collection
$("#collection").change(function(){
		var collection  = this.options[this.selectedIndex].value ;
	currentCollection = collection;
	show_records(records, collection);
});

// change the record when select the number
$("#formula_number").change(function(){
		var fn  = this.options[this.selectedIndex].value ;
	show_record_to_edit(records, currentCollection, fn);
});

// submit edited record to database
$('#submit_edit_record').click(function(event){
    event.preventDefault();
	var data = $('#edit_record').serializeArray().reduce(function(obj, item) {
			obj[item.name] = item.value;
			return obj;
	}, {});
	edit(data);

});

// do the edit, submit to app.py
function edit(data) {
	console.log(data);
    var url = '/api/records/' + data.collection;	
	$.ajax({
	   url: url,
	   type: 'PUT',
	   data: JSON.stringify(data), 
	   dataType: 'json',
      contentType: 'application/json; charset=utf-8',
	   complete: function(d){
	    	//var res = JSON.parse(d.responseText)
	    	console.log(d);
	        get_records();
	   },
    });
}


// delete the record, calls to app.py
function deleteRecord(data){
	var url = '/api/records/' + data.collection + '/' + data.formula_number; 
	$.ajax({
	   url: url,
	   type: 'DELETE',
	   data: JSON.stringify(data), 
	   dataType: 'json',
               contentType: 'application/json; charset=utf-8',
	   complete: function(d){
	    	var res = JSON.parse(d.responseText)
		    console.log(res);
	        get_records();
	   },
	});
}

// load records on initial page load
get_records();

// ask to confirm if request to delete record
$(document).on( 'click', '#submit_delete_record', function () {
	var data = $('#edit_record').serializeArray().reduce(function(obj, item) {
			obj[item.name] = item.value;
			return obj;
	}, {});
	if (confirm("Want to delete record" + data.formula_number+ ' ?')){
		deleteRecord(data);
	}
	$('#submit_delete_record').blur();
	get_records();
});


