/// this file is exactly the same as permanent.js except for changing some of the names
// it's used for the exclusion collection

// if need notes, docs, see permanent.js



    var allSnapshots = [];
    var allPermanent = [];
    var currentSnapshot = {
        name: 'Permanent Exclusion',
        excluded: [],
        previous_spot: 1,
    };
    var table;
    window.actions = [];
    


    function records(){
      $.ajax({
       url: '/api/records',
       type: 'get',
       complete: function(d){
         d = JSON.parse(d.responseText)['results'];
         collection = Object.keys(d)[0]
         show_collections(d);
     },
 });
  }

  function permanent(prev){
      $.ajax({
       url: '/api/permanent_exclusion',
       type: 'get',
       complete: function(d){
         res = JSON.parse(d.responseText);
         var state = res.state;

         var date;
         if (state){
            date = state.updated;
        }
        d = res.results;
        allPermanent = d;
        show_results(d, 'permanent');
        if (prev) {
            prevSpot(prev);
        }

        currentSnapshot.date = date;
        currentSnapshot.name = 'Permanent Exclusion';
        displayCurrent();
    },
});
  }

  function displayCurrent(){
    var ddate;
        // arbitrarily below 2010 then not date
        if (!currentSnapshot.date || new Date(currentSnapshot.date) < new Date(2010, 0, 2)){
            ddate = 'No Saved Version'
        } else {
            ddate = new Date(currentSnapshot.date);
        }
        document.getElementById('last_saved').innerHTML = ddate; 
        document.getElementById('current_name').innerHTML = currentSnapshot.name 
        document.getElementById('current_excluded').innerHTML = currentSnapshot.excluded.join(); 
    }


    function add(id, prev){
      $.ajax({
       url: "/api/permanent_exclusion",
       type: 'POST',
       dataType: 'json',
       contentType: 'application/json; charset=utf-8',
       data: JSON.stringify({'formula_numbers': id.toString(), 'action': 'add'}), 
       complete: function(d){
         permanent(prev);
     },
 });
  }

  function remove(id, prev){
      $.ajax({
       url: "/api/permanent_exclusion",
       type: 'POST',
       dataType: 'json',
       contentType: 'application/json; charset=utf-8',
       data: JSON.stringify({'action': 'remove', 'formula_numbers': id.toString()}), 
       complete: function(d){
         console.log(d);
         permanent(prev);
     },
 });
  }


  function show_results(data,id){
    $("#currentRecords").html(data.map(function(d){return d.formula_number}).sort(function(a,b){return a - b}).join());

    var columns = [
    {"data":"formula_number", "title": "F#"},
    {"data":"raw_formulas[<br>]", "title": "Fs"},
    {"data":"comments[<br>]", "title": "Cs"},
    {"data":"category" , "title": "C"},
    {"data":"Q[<br>]", "title": "Q"},
    {"data":"N[<br>]", "title": "N"},
    {"data":"S[<br>]", "title": "S"},
    {"data":"R[<br>]", "title": "R"},
    {"data":"I[<br>]", "title": "I"},
    {"data":"date", "title": "D"},
    ]


    table = $('#' + id).DataTable({
        "bDestroy": true,
        processing: true,
        "lengthMenu": [ 1],
        "dom": '<"top"<"actions">fp<"clear">><"clear">rt<"bottom">',
        "bAutoWidth": false,

        "aoColumnDefs": [
        { "sClass": "large-bold", "aTargets": [0] },
        { "sClass": "bold-face", "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 8] },
        { "sWidth": "10%", "aTargets": [2] },
        { "sWidth": "10%", "aTargets": [1] },
        { "sWidth": "1%", "aTargets": [ 0, 3, 4, 5,6, 7,8, 9] },
        ],
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if (aData['permanent'] == true){
                jQuery('td:eq(0)', nRow).addClass('redText');
            }
            var cellData = myEscaper(aData['raw_formulas']); 
            jQuery('td:eq(1)', nRow).html(cellData);
            var cellData2 = myEscaper(aData['comments']); 
            jQuery('td:eq(2)', nRow).html(cellData2);
            return nRow;
        },
        data: data,
        columns: columns,
    });

    $('#' + id + ' '  + 'th').css("max-width","100px")

}

var tagsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};

function replaceTag(tag) {
    return tagsToReplace[tag] || tag;
}

function safe_tags_replace(str) {
    return str.replace(/[&<>]/g, replaceTag);
}

function myEscaper(ls){
    var newLs = [];
    ls.forEach(function(d){
        newLs.push(safe_tags_replace(d) + '<br>');
    });
    return newLs;
}


$("#submit_remove_record").click(function(){
   var val = document.getElementById("remove_record").value;
   remove(val);
   $('#submit_remove_record').blur()
   permanent();
});

$("#submit_add_record").click(function(){
   var val = document.getElementById("add_record").value;
   add(val);
   $('#submit_add_record').blur()
   permanent();
});

function show_snapshots(data) {
  var ul = document.getElementById("snapshots");
  var li_str = "";
  li_str += '<li value="current" class="active"><a id="current" href="#">Exclusions</a></li>';
  data.forEach( function(d) {
   li_str += '<li value="' + d.name + '"><a id="'+d.name+'"href="#">' + d.name + '</a></li>';
});
  ul.innerHTML = li_str;
}

function snapshots(){
  $.ajax({
   url: '/api/permanent_exclusion/snapshots',
   type: 'get',
   complete: function(d){
     d = JSON.parse(d.responseText)['results'];
     console.log(d);
     allSnapshots = d; 
     show_snapshots(d);
 },
});
}

$("#submit_create_snapshot").click(function(){
   var n = document.getElementById('permanent_snapshot_name').value;
   create_snapshot(n);
   $('#submit_create_snapshot').blur()
});


$('#delete_permanent_records').click(function(){
    if (confirm('Are you sure you would like to delete the permanent records?')){
      $.ajax({
       url: "/api/permanent_exclusion",
       type: 'DELETE',
       complete: function(d){
         d = JSON.parse(d.responseText)['results'];
         console.log(d);
         permanent();
     },
 });
  }
  $('#delete_permanent_records').blur()

});

$('#delete_permanent_snapshots').click(function(){
    if (confirm('Are you sure you would like to delete the snapshots?')){
      $.ajax({
       url: "/api/permanent_exclusion/snapshots",
       type: 'DELETE',
       complete: function(d){
         d = JSON.parse(d.responseText)['results'];
         console.log(d);
         snapshots();
     },
 });
  }
  $('#delete_permanent_snapshots').blur()

});


function create_snapshot(id){
  $.ajax({
   url: "/api/permanent_exclusion/snapshots",
   type: 'POST',
   dataType: 'json',
   contentType: 'application/json; charset=utf-8',
   data: JSON.stringify({name: id, excluded: currentSnapshot.excluded}),
   complete: function(d){
     d = JSON.parse(d.responseText)['results'];
     console.log(d);
     snapshots();
 },
});
}

$(document).on("click", "#snapshots a", function(){
  var id = this.id;
  if (id == 'current'){
   permanent();
   snapshots();
   $('#add-remove-space').show();
} else {
 $('#snapshots li').removeClass('active');
 $(this).parent().addClass('active');
 $(this).blur();
 console.log(allSnapshots);
 var w = allSnapshots.filter(function(d){
     if(d.name == id){
        return true
    }		
    else{
        return false
    }
})[0];
 show_results(w['records'], 'permanent');
 currentSnapshot = w;
 $('#add-remove-space').hide();
}
displayCurrent();
});


function prevSpot(old, clicks){
    var current;
    $("#permanent tbody tr").each(function(i) {
     current = parseInt($(this).find("td:first").text());
 });
    if (current < old){
        if (clicks === undefined || clicks < allPermanent.length){
            clicks = clicks || 0;
            $("#permanent_next").click();
            clicks ++;
            prevSpot(old, clicks);
        }
    }
}


$(document).keyup(function(e) {

    var foc = false;
    $(document).find(':input').each(function(){
        if ($(this).is(":focus")){
            foc = true;
        }
    })

    if (!foc){

       if(e.which == 13) {
           $("#permanent_next").click();
       }

       if(e.which == 98) {
           $("#permanent_previous").click();
       }

       if(e.which == 92 || e.which == 220 || e.which == 191) {
        var kill;
        var kill_id;
        $("#permanent tbody tr").each(function(i) {
            console.log(i);
            kill = parseInt($(this).find("td:first").text());
            table.row($(this)).remove().draw();
        });
        if ( !(currentSnapshot.excluded.indexOf(kill) > -1)){
            currentSnapshot.excluded.push(kill);
        }
        remove(kill, kill);
        window.actions.push({action: 'exclude', date: new Date(), 'record': kill});
    }


    if (e.which == 82 || e.which == 85){
        if (actions.length > 0){
            var prev = window.actions.sort(function(a,b){return a.date.getTime() - b.date.getTime()}).pop();
            if (prev.action == 'exclude'){
                var killed = currentSnapshot.excluded.pop();
                add(killed, killed);
            }
        }
    }
}
});

    snapshots();
    permanent();


    // #myInput is a <input type="text"> element
    $('#search-text').on( 'keyup', function () {
        console.log(this.value);
        var term = '\\b' + this.value + '\\b'; 
        table.column(0).search( term, true, false).draw();
    } );

    $("#delete-snapshot").click(function(){
        var name = currentSnapshot.name;
        var data = {'name': name};
        if (confirm('Are you sure you want to delete ' + name+ '?')){
          $.ajax({
           url: "/api/permanent_exclusion/snapshots",
           type: 'DELETE',
           dataType: 'json',
           contentType: 'application/json; charset=utf-8',
           data: JSON.stringify(data),
           complete: function(d){
            permanent();
            snapshots();
            console.log(d);
        },
    });
      }
      $("#delete-snapshot").blur();

  });


    $('#add-back').click(function(){
        var records = currentSnapshot.records.map(function(d){
            return d.formula_number;
        }).join();
        add(records);
        window.location.href = '/permanent_exclusion';
        alert('records added!');
    });

