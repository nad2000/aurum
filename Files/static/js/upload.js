

$(document).ready(function(){

    $('#upload_file_button').click(function(e){

        e.preventDefault();
        var formData = new FormData();
        formData.append('file', $('#file')[0].files[0]);
        Pace.start;
        console.log('uploading...');
        $.ajax({
               url : '/api/upload',
               type : 'POST',
               data : formData,
               processData: false,  // tell jQuery not to process the data
               contentType: false,  // tell jQuery not to set contentType
               success : function(data) {
                   alert(data.message + ' uploaded');
               },
               error: function(data){
                    alert('Error while uploading! File not uploaded!');
                }
        });
    
    
    });

});
