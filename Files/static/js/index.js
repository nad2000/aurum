
	
// this is for the All Collections page
// general queries against all of the records in the database

/// mostly Jquery Datatables

var table;

var currentQuery = {
	'results': [],
	'query': '',
	'colection': '',
}; 

// call the database with a query
function query(val, collection) {
     var data = {'query': val};
         var url = '/api/records/' + collection;
	$.ajax({
	   url: url,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
	   complete: function(d){
		var res = JSON.parse(d.responseText)
		currentQuery = res;
		show_results(res['results'], 'query_results');
		showCurrents(currentQuery);
	   },
    });
}


// get all the records
function records(){
	$.ajax({
	   url: '/api/records',
	   type: 'get',
	   complete: function(d){
		d = JSON.parse(d.responseText)['results'];
		currentQuery['collection'] = Object.keys(d)[0]
		show_collections(d);
	   },
      });
}


// need to escape some stuff in the records that looks like html
/////////////////////////////////////////////////////

var tagsToReplace = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};

function replaceTag(tag) {
    return tagsToReplace[tag] || tag;
}

function safe_tags_replace(str) {
    return str.replace(/[&<>]/g, replaceTag);
}

function myEscaper(ls){
var newLs = [];
ls.forEach(function(d){
        newLs.push(safe_tags_replace(d) + '<br>');
});
return newLs;
}

/////////////////////////////////////////////////////


// show the query results 
function show_results(data,id){

    var columns = [
    {"data":"formula_number", "title": "F#"},
    {"data":"raw_formulas[<br>]", "title": "Fs"},
    {"data":"comments[<br>]", "title": "Cs"},
    {"data":"category" , "title": "C"},
    {"data":"collection" , "title": "COL"},
    {"data":"Q[<br>]", "title": "Q"},
    {"data":"N[<br>]", "title": "N"},
    {"data":"S[<br>]", "title": "S"},
    {"data":"R[<br>]", "title": "R"},
    {"data":"I[<br>]", "title": "I"},
    {"data":"date", "title": "D"},
        ]


    table = $('#' + id).DataTable({
    "bDestroy": true,
    processing: true,
    "lengthMenu": [ 1],
    "dom": '<"top"<"actions">fp<"clear">><"clear">rt<"bottom">',
    "bAutoWidth": false,

    "aoColumnDefs": [
          { "sClass": "large-bold", "aTargets": [0] },
          { "sClass": "bold-face", "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] },
          { "sWidth": "300px", "aTargets": [2] },
          { "sWidth": "100px", "aTargets": [1] },
          { "sWidth": "1%", "aTargets": [ 0,4, 5,6, 7,8, 9, 10] },
        ],
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                jQuery('td:eq(10)', nRow).addClass('rotate');
        if (aData['permanent'] == true){
                jQuery('td:eq(0)', nRow).addClass('redText');
        }
        var cellData = myEscaper(aData['raw_formulas']); 
        jQuery('td:eq(1)', nRow).html(cellData);
        var cellData2 = myEscaper(aData['comments']); 
        jQuery('td:eq(2)', nRow).html(cellData2);
                return nRow;
            },
        data: data,
        columns: columns,
    });

    $('#' + id + ' '  + 'th').css("max-width","100px")

}

// show the results of teh query    
function show_query(val) {
    if (typeof val == undefined){
	val = 'All Records'
	    }
    var q = document.getElementById("currentQuery");
        q.innerHTML = val;
}

// show which collections we can pick
function show_collections(data) {
	var sel = document.getElementById("collection");
            var options_str = "";
	Object.keys(data).forEach( function(d) {
			options_str += '<option value="' + d + '">' + d + '</option>';
	});
	sel.innerHTML = options_str;
}

// show stats of the current results
function showCurrents(d){
	var display = JSON.parse(JSON.stringify(d));
	console.log(d);
	if (display['query'].length == 0){
		display['query'] = "All Records";
	} 
	document.getElementById('currentQuery').innerHTML = display['query'];

}

// handle when we change collection
$("#collection").change(function(){
		currentQuery['collection'] = this.options[this.selectedIndex].value ;
});

// submit a query handler
$("#submit_query").click(function(){
    var val = document.getElementById("query").value;
    query(val, currentQuery['collection']);
    $('#submit_query').blur()
});

// handle keystokes
$(document).keypress(function(e) {
    if(e.which == 13) {
        $("#query_results_next").click();
    }
});


// pull everything on initial load	    
records();


// handling the search box
$(document).on( 'click', '#submit_search_formula_number', function () {
	var val = document.getElementById('search_formula_number').value;
	console.log(val);
  		table.columns(0).search(val).draw() ;
	$('#submit_search_formula_number').blur();
});
    
// #myInput is a <input type="text"> element
$('#search-text').on( 'keyup', function () {
    table.search( this.value ).draw();
} );

// #myInput is a <input type="text"> element
$('#search-formula').on( 'keyup', function () {
    var term = '\\b' + this.value + '\\b'; 
    table.column(0).search( term, true, false).draw();
} );
