
// this is for deleting collections
// pretty simple

	var records = {};
	var currentCollection = '';


	function show_collections(data) {
		var sel = document.getElementById("collection");
                var options_str = "";
		Object.keys(data).forEach( function(d) {
			if (d != 'all'){
  				options_str += '<option value="' + d + '">' + d + '</option>';
			}
		});
		sel.innerHTML = options_str;
	}


        function get_records(){
		$.ajax({
		   url: '/api/records',
		   type: 'get',
		   complete: function(d){
			d = JSON.parse(d.responseText)['results'];
			show_collections(d);
			console.log(d);
			records = d;
			currentCollection = Object.keys(d)[1];
		   },
	      });
	}



	$('#submit_delete_collection').click(function(){
		var data = $('#delete_collection').serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
		}, {});
		deleteCollection(data);

	});


	function deleteCollection(data) {
		console.log(data);
             var url = '/api/records/' + data.collection;	
		$.ajax({
		   url: url,
		   type: 'DELETE',
		   complete: function(d){
			var res = JSON.parse(d.responseText)
			console.log(res);
			alert(data.collection + ' has been deleted');
			get_records();
		   },
	    });
	}

	get_records();


