// file does all of the database export / loading from the database page

var Collections;
var Options = [
    ['Include All','all'],
    ['Collections', 'collections'],
    ['Permanent Collection & Snapshots', 'perm'],
    ['Exclusion Collection & Snapshots', 'permex'],
    ['Working Files', 'working'],
    ['Summaries', 'sum'],
    ['Saved Paths', 'paths'],
];

// pull collections from the database
(function collections(){
	$.ajax({
	   url: '/api/collections?everything=true',
	   type: 'get',
	   success: function(d){
		res = d.results;
        Collections = res;
	   },
    });
})();


// build checklist of things we can do with the database (which things to include, not, etc)
(function buildChecks(){
    var arr = Options;
    console.log(arr);
    var str = '';   
    arr.forEach(function(d){
        str += '<div class="input-group input-group-sm">';
        str += '<span class="input-group-btn">';
        str += '<input type="checkbox" name="'+d[1]+'"/></span>';
        str += '<label>'+d[0]+'</label>';
        str += '</div>';
    });
    $('#save_checklist').html(str);
    $('#export_checklist').html(str);
})();


// handle when user says delete a db
$('#delete-db').click(function (){
    if (confirm('Are you sure you want to delete everything?')){
	    $.ajax({
	       url: "/api/database",
	       type: 'DELETE',
	       success: function(d){
                alert('Database deleted!');
	       },
	    });
    }
    $('delete-everything').blur();
});


// submit an export
$('#submit_save_database').click(function(e){
    e.preventDefault();
	var data = $('#save_database').serializeArray().reduce(function(obj, item) {
    	obj[item.name] = item.value;
    	return obj;
	}, {});
    console.log(data);
    var url = '/api/save_database';
	$.ajax({
		url: url,
		type: 'POST',
		data: JSON.stringify(data), 
		dataType: 'json',
        contentType: 'application/json; charset=utf-8',
		success: function(d){
            console.log(d);
                alert('Data exported successfully!');
		},
	});
    $('#submit_save_database').blur();
});

// request to load a database
$('#submit_load_database').click(function(e){
    e.preventDefault();
	var data = $('#load_database').serializeArray().reduce(function(obj, item) {
    	obj[item.name] = item.value;
    	return obj;
	}, {});
    var url = '/api/load_database';
	$.ajax({
		url: url,
		type: 'POST',
		data: JSON.stringify(data), 
		dataType: 'json',
        contentType: 'application/json; charset=utf-8',
		success: function(d){
                alert('Data loaded successfully!');
		},
	});
    $('#submit_load_database').blur();
});

