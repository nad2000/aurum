
// file for creating records on create record page
// very simple no comments

function show_collections(data) {
	var sel = document.getElementById("collection");
            var options_str = "";
	Object.keys(data).forEach( function(d) {
		if (d != 'all'){
				options_str += '<option value="' + d + '">' + d + '</option>';
		}
	});
	sel.innerHTML = options_str;
}

    function records(){
	$.ajax({
	   url: '/api/records',
	   type: 'get',
	   complete: function(d){
		d = JSON.parse(d.responseText)['results'];
		show_collections(d);
	   },
      });
}


$('#submit_create_record').click(function(){
	var data = $('#create_record').serializeArray().reduce(function(obj, item) {
			obj[item.name] = item.value;
			return obj;
	}, {});
		var selectedValues = [];    
		$("#collection :selected").each(function(){
    		selectedValues.push($(this).val()); 
		});
	if (data.new_collection.length > 1){
		data.collection = data.new_collection;
		create(data);
	} 
	else {
		selectedValues.forEach(function(d){
			data.collection = d;
			create(data);
	});
	}

});


function create(data) {
	console.log(data);
         var url = '/api/records/' + data.collection;	
	$.ajax({
	   url: url,
	   type: 'POST',
	   data: JSON.stringify(data), 
	   dataType: 'json',
               contentType: 'application/json; charset=utf-8',
	   complete: function(d){
		var res = JSON.parse(d.responseText)
		console.log(res);
		alert(JSON.stringify(res));
	   },
    });
}

records();


