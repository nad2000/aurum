// This file and others is effectively a big state machine. 
// If you were using React, the currentWorking would be 
// the main application state

// Generally speaking, we do an action to do the state and re-render everything.
// This would be far better in React or even Angular


// the point of working.js is this -- 
// we have this huge database of files
// we want to query this database according to some parameters
// then in working.js, the user manually curates this list

// every once and and while, the user can add records to other collections
// like permanent or exclusion, and these records can be added or excluded
// in future queried datasets


var key = false;
var table;
var table2;
var readyToSave = false;
var Perm = [];
var PermEx = [];
var Collections = [];
var Queryable = [];


function getOriginalWorking(){
    return {
        'original_results': [],
        'results': [],
        'query': '',
        'name': 'current', 
        'excluded': [],
        'outside': [],
        'merge_permanent': false,
        'merge_permanent_exclusion': false,
        'collection': '',
        'previous_spot': 1,
        'last_saved': null,
        'last_search': null,
        'last_search_value': null,
        'last_filter': null,
        'last_filter_value': null,
    }; 

}

// get a clean blank object that we will use as our main state
currentWorking = getOriginalWorking();

function getState(){
    var p;
    $.ajax({
     url: '/api/state',
     type: 'get',
     async: false,
     success: function(d){
        p = d;
    },
});
    return p;
}

// used to check what's in our perm and exclusion databases

function getPerm(){
    var p;
    $.ajax({
     url: '/api/permanent',
     type: 'get',
     async: false,
     success: function(d){
        p = d.results;
        Perm = p;
    },
});
    return p;
}

function getPermEx(){
    var p;
    $.ajax({
     url: '/api/permanent_exclusion',
     type: 'get',
     async: false,
     success: function(d){
        p = d.results;
        PermEx = p;
    },
});
    return p;
}


jQuery.fn.dataTable.Api.register( 'page.jumpToData()', function ( data, column ) {
    var pos = this.column(column, {search: 'applied', order:'applied', filter: 'applied'}).data().indexOf( data );
    if ( pos >= 0 ) {
        var page = Math.floor( pos / this.page.info().length );
        this.page( page ).draw( false );
    } else {
        if (currentWorking.previous_index){
            this.page( currentWorking.previous_index).draw( false );
        }
    }
    return this;
});

// get a working files

function getWorking(name){
    $.ajax({
     url: '/api/working/' + name,
     type: 'get',
     success: function(d){
        currentWorking = JSON.parse(d) || getOriginalWorking();
        currentWorking.last_viewed = new Date();
        setCurrent();          
        }
    }); 
}

// set the state 
function setCurrent(){
    console.log('setting current...');
    Pace.start();
    factorExcludes();
    try{
        currentWorking.excluded = currentWorking.excluded.sort(function(a,b){return a - b}); 
    }catch(e){
    }
    if (currentWorking.merge_permanent){
        mergePermanent();
    }
    if (currentWorking.merge_permanent_exclusion){
        mergePermanentExclusion()
    }

    // we call this too much
    var p = getPerm();
    var currentFormulas = currentWorking.results.map(function(x){return x.formula_number});
    var currentPermFormulas = p.map(function(d){return d.formula_number});
    currentWorking.results.forEach(function(d){
        var fn = d.formula_number;
        if (currentPermFormulas.indexOf(fn) > -1 ){
            d.permanent = true;
        }
    });
    show_results();
    var last_search = currentWorking.last_search;
    if (last_search){
        table.column(0).search( last_search, true, false).draw(false);
        table2.column(0).search( last_search, true, false).draw(false);
        $('.search-text').val(currentWorking.last_search_value);
    }
    var last_filter = currentWorking.last_filter;
    if (last_filter){
        table.column(3).search( last_filter, true, false).draw(false);
        table2.column(3).search( last_filter, true, false).draw(false);
        $('.cat-filter').val(currentWorking.last_filter_value);
    } else {
        var term = "1|2|3|4";
        table.column(3).search( term, true, false).draw(false);
        table2.column(3).search( term, true, false).draw(false);
        $('.cat-filter').val(0);
    }
    prevSpot(currentWorking.previous_spot);
    readyToSave = true;
    showCurrents();	
    Pace.stop();
    console.log("EXCLUDED:", currentWorking.excluded);
}

// query the database (calls app.py)
function query(val, excluded, outside, collection, show_only, previous) {
    collection = $('#collection').find(":selected").text() || collection;
    $('.cat-filter').val(0);
    $('.search-text').val('');
    currentWorking.merge_permanent = false;
    currentWorking.merge_permanent_exclusion = false;
    currentWorking.last_saved =  null;
    currentWorking.last_search = null;
    currentWorking.last_search_value = null;
    currentWorking.last_filter =  null;
    currentWorking.last_filter_value = null;
    currentWorking.merged_files = []; //because now the merge is not relevant!
    var data = {
        'query': val, 
        'excluded': excluded.join(), 
        'outside': outside.join(), 
        'merge_permanent': currentWorking.merge_permanent, 
        'merge_permanent_exclusion': currentWorking.merge_permanent_exclusion,
        'collection': collection,
        'currentWorking': currentWorking,
    }
    var url = '/api/records/' + collection;
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        success: function(res){
            if (res.results == undefined || !res.results){
                res.results = [];
            }
            currentWorking.results = res.results;
            currentWorking.original_results = res.results.slice();
            currentWorking.query = res.query || '';
            currentWorking.collection = res.collection ;
            setCurrent();
            if (previous){prevSpot(previous);}

        },
    });
}

// get all of our collections
function collections(){
	$.ajax({
        url: '/api/collections',
        type: 'get',
        success: function(d){
          res = d.results;
          Collections = res.filter(function(d){
            return (
                !(d.indexOf('State') > -1) &&
                !(d.indexOf('perm') > -1) &&
                !(d.indexOf('path') > -1) 
                )
        });
      },
  });
}

// get our working file by name
function working(jumpToFile){
	$.ajax({
        url: '/api/working',
        type: 'get',
        success: function(d){
          allWorking = d.results || []; 
          otherLastViewed = allWorking.sort(
            function(a,b){
                var c = new Date(b.last_viewed).getTime() || 0;    
                var d =  new Date(a.last_viewed).getTime() || 0;
                return  c - d  ;
            })[0].name || 'current'; 
          if (jumpToFile){
            lastViewed = jumpToFile;
            show_working(lastViewed);
        }else{
            var rs = getState().last_viewed 
            //lastViewed = rs || 
            lastViewed = 'current';
            show_working(lastViewed);
            getWorking(lastViewed);
        }
    },
});
}


// get all collections that are queryable 
function getQueryable(){
	$.ajax({
        url: '/api/queryable',
        type: 'get',
        success: function(d){
            Queryable = d.results;
            show_collections();
            show_collection_shortcuts();
        },
    });
}


// save the working file on the database
function save_working(no_refresh, background, name, completely_silent){
    getSpot();
    currentWorking.last_saved = new Date();
    var data = jQuery.extend(true, {}, currentWorking);
    if (name) {
        data.name = name;
    }
    $.ajax({
        url: "/api/working",
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        success: function(d){
                if (!background){
                    working(currentWorking.name);
                }
                currentWorking.merge_permanent = d.merge_permanent; 
                currentWorking.merge_permanent_exclusion = d.merge_permanent_exclusion;
                collections();
                getQueryable();
                showCurrents();
        },
    });
}

// add a record to the permanent collection
function add(id){
	$.ajax({
        url: "/api/permanent",
        type: 'post',
        datatype: 'json',
        contenttype: 'application/json; charset=utf-8',
        data: JSON.stringify({'formula_numbers': id, 'action': 'add'}), 
        complete: function(d){
        },
    });
}

// display the results ( this is like a render in react)
// this is primarily a Jquery Datatable
// we have two tables due to some bugs in the way Datatables
// shares info across tables on the page (they need to be in sync)


function show_results(){
    var id = 'query_results';
    var data = currentWorking.results;
    if (data){
        data.sort(function(a, b){return b.formula_number - a.formula_number});
    } 

    // calculate the red green pcts
    data.forEach(function(d){
        var reds = [];
        var greens = [];
        ['Q', 'N', 'S', 'R', 'I'].forEach(function(c){
            var redNum = parseFloat(d[c][3]);
            var greenNum = parseFloat(d[c][2]);
            if (!isNaN(redNum) && !isNaN(greenNum)) {
                reds.push(redNum);
                greens.push(greenNum);
            }
        });
        var greens_sum = greens.reduce(function(a, b) { return a + b; }, 0);
        var reds_sum = reds.reduce(function(a, b) { return a + b; }, 0);
        var greens_len = greens.length;
        var reds_len = reds.length;
        var green_red_total = greens_sum + reds_sum;
        var green_red_pct;
        try {
            green_red_pct = greens_sum/green_red_total;
        } catch(e) {
            green_red_pct = 0;
        }
        green_red_pct = (green_red_pct * 100).toFixed(1);
        d.green_red_pct = green_red_pct;
    });


    var columns = [
    {"data":"formula_number", "title": "F#"},
    {"data":"raw_formulas[<br>]", "title": "Fs"},
    {"data":"comments[<br>]", "title": "Cs"}, 
    {"data":"category" , "title": "C"},
    {"data": 'green_red_pct', "title": "Pct"},
    {"data":"Q", "title": "Q"},
    {"data":"N", "title": "N"},
    {"data":"S", "title": "S"},
    {"data":"R", "title": "R"},
    {"data":"I", "title": "I"},
    {"data":"date", "title": "D"},
    ]

    table = $('.' + id).DataTable({
       "bDestroy": true,
       processing: true,
       "lengthMenu": [ 1],
       "dom": '<"top"<"actions">fp<"clear">><"clear">rt<"bottom">',
       "bAutoWidth": false,
       "aoColumnDefs": [
       { "sClass": "large-bold align-top", "aTargets": [0] },
       { "sClass": "bold-face align-top", "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] },
       { "sWidth": "15%", "aTargets": [2] },
       { "sWidth": "15%", "aTargets": [1] },
       { "sWidth": "10%", "aTargets": [4] },
       { "sWidth": "1%", "aTargets": [ 0, 3, 5 ,6, 7,8, 9, 10] },
       ],
       "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          if (aData['permanent'] == true){jQuery('td:eq(0)', nRow).addClass('redText');}
          var cellData = myEscaper(aData['raw_formulas']); 
          jQuery('td:eq(1)', nRow).html(cellData);
          var ls = ['Q', 'N', 'S', 'R', 'I'].reverse();
          var count = 9;
          ls.forEach(function(d){
            var c = aData[d];
            var h;
            try{
                h = [
                '<tr><td class="">'+ c[0] +'</td></tr>',
                '<tr><td class="">'+ c[1] +'</td></tr>',
                '<tr><td class="greenbold">'+ c[2] +'</td></tr>',
                '<tr><td class="redbold">'+ c[3] +'</td></tr>',
                '<tr><td class="">'+ c[4] +'</td></tr>',
                ].join();
            } catch(e){
                h = '';
            }
            jQuery('td:eq('+count+')', nRow).html(h);
            count = count - 1;

        });
          var green_red_html = [
          '<tr><td class="">'+ '' +'</td></tr>',
          '<tr><td class="">'+ '' +'</td></tr>',
          '<tr><td class="bluebold largefont">'+ aData.green_red_pct +'%</td></tr>',
          '<tr><td class="bluebold largefont">'+ ''+'</td></tr>',
          '<tr><td class="">'+ '' +'</td></tr>',
          ].join();

          jQuery('td:eq(4)', nRow).html(green_red_html);
          var cellData2 = myEscaper(aData['comments']); 
          jQuery('td:eq(2)', nRow).html(cellData2);
          return nRow;
      },
      data: data.map(function(d){d.DT_RowId = d.formula_number; return d;}),
      columns: columns,
  });

table2 = $('.second').DataTable({
   "bDestroy": true,
   processing: true,
   "lengthMenu": [ 1],
   "dom": '<"top"<"actions">fp<"clear">><"clear">rt<"bottom">',
   "bAutoWidth": false,
   "aoColumnDefs": [
   { "sClass": "large-bold", "aTargets": [0] },
   { "sClass": "bold-face", "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] },
   { "sWidth": "15%", "aTargets": [2] },
   { "sWidth": "15%", "aTargets": [1] },
   { "sWidth": "10%", "aTargets": [4] },
   { "sWidth": "1%", "aTargets": [ 0, 3, 5 ,6, 7,8, 9, 10] },
   ],
   "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      if (aData['permanent'] == true){jQuery('td:eq(0)', nRow).addClass('redText');}
      var cellData = myEscaper(aData['raw_formulas']); 
      jQuery('td:eq(1)', nRow).html(cellData);
      var ls = ['Q', 'N', 'S', 'R', 'I'].reverse();
      var count = 9;
      ls.forEach(function(d){
        var c = aData[d];
        var h;
        try{
            h = [
            '<tr><td class="">'+ c[0] +'</td></tr>',
            '<tr><td class="">'+ c[1] +'</td></tr>',
            '<tr><td class="greenbold">'+ c[2] +'</td></tr>',
            '<tr><td class="redbold">'+ c[3] +'</td></tr>',
            '<tr><td class="">'+ c[4] +'</td></tr>',
            ].join();
        } catch(e){
            h = '';
        }
        jQuery('td:eq('+count+')', nRow).html(h);
        count = count - 1;

    });
      var green_red_html = [
      '<tr><td class="">'+ '' +'</td></tr>',
      '<tr><td class="">'+ '' +'</td></tr>',
      '<tr><td class="bluebold largefont">'+ aData.green_red_pct +'%</td></tr>',
      '<tr><td class="bluebold largefont">'+ ''+'</td></tr>',
      '<tr><td class="">'+ '' +'</td></tr>',
      ].join();

      jQuery('td:eq(4)', nRow).html(green_red_html);
      var cellData2 = myEscaper(aData['comments']); 
      jQuery('td:eq(2)', nRow).html(cellData2);
      return nRow;
  },
  data: data.map(function(d){d.DT_RowId = d.formula_number; return d;}),
  columns: columns,
});
$('#' + id + ' '  + 'th').css("max-width","100px")
}


// display the query 
function show_query(val) {
    if (typeof val == undefined){
       val = 'All Records'
   }
   var q = document.getElementById("currentQuery");
   q.innerHTML = val;
}


// show the working files that we can select from on the side of the screen
function show_working(active) {
    var data = allWorking;
    var ul = document.getElementById("working");
    var li_str = "<p style='padding:15px;font-weight:bold;text-decoration:underline;' >Working Files</p>";
    var lis = [];
    data.sort(function(a, b){return new Date(b.date).getTime() - new Date(a.date).getTime()});
    var lis = [];
    if (active == 'current'){
       li_str += '<li class="active" value="current"><a id="current">Working File</a></li>';
   } else {
       li_str += '<li value="current"><a id="current">Working File</a></li>';
   }
   data.forEach( function(d) {
    if (d.name != 'current'){
        if (active == d.name){
            var cl = 'active';
        } else {
            var cl = '';
        }
        li_str += '<li class="'+ cl +'" value="' + d.name + '"><a id="'+d.name+'">' + d.name + '</a></li>';
    }
});
   ul.innerHTML = li_str;
}

// helper shortcut links on the side of the screen for common operations
function show_collection_shortcuts() {
    var data = Collections;
    var ul = document.getElementById("collectionShortcuts");
    var li_str = "<p style='padding:15px;font-weight:bold;text-decoration:underline;' >Collection Shortcuts</p>";
    var lis = [];
    data.forEach( function(d) {
        var cl = '';
        li_str += '<li class="'+ cl +'" value="' + d + '"><button class="btn btn-default navbar-btn btn-sm" id="'+d+'">' + d + '</button></li>';
    });
    ul.innerHTML = li_str;
}

// display collections that can be selected in the dropdown
function show_collections() {
	var sel = document.getElementById("collection");
    var options_str = "";
    var data = Queryable;
    data.forEach( function(d) {
       options_str += '<option value="' + d + '">' + d + '</option>';
   });
    sel.innerHTML = options_str;
    $('#collection').val('working_file:current');
}

// escaping for html
var tagsToReplace = {
   '&': '&amp;',
   '<': '&lt;',
   '>': '&gt;'
};

// escaping for html
function replaceTag(tag) {
   return tagsToReplace[tag] || tag;
}

// escaping for html
function safe_tags_replace(str) {
   return str.replace(/[&<>]/g, replaceTag);
}

// escaping for html
function myEscaper(ls){
	var newLs = [];
	ls.forEach(function(d){
		newLs.push(safe_tags_replace(d) + '<br>');
	});
	return newLs;
}

// render the current state 
function showCurrents(d){
    console.log('showing currents...'); 
    document.getElementById('currentExcluded').innerHTML = currentWorking.excluded.join();
    document.getElementById('numberExcluded').innerHTML = currentWorking.excluded.length;
    document.getElementById('currentOutside').innerHTML = currentWorking.outside;
    document.getElementById('currentCollection').innerHTML = currentWorking.collection;
    var nm;
    nm = currentWorking.name;
    if (nm == 'current'){
        nm = 'Working File';
    } 
    document.getElementById('currentWorkingFileName').innerHTML = nm;
    document.getElementById('previous_spot').innerHTML = currentWorking.previous_spot;
    var ls = new Date(currentWorking.last_saved) || null; 
    if (!ls || ls < new Date(2010, 1, 1)){ls = 'n/a'};
    document.getElementById('last_saved').innerHTML = ls;
    document.getElementById('currentQuery').innerHTML = safe_tags_replace(currentWorking.query);
    document.getElementById("merge_permanent").checked = currentWorking.merge_permanent;
    document.getElementById("merge_permanent_exclusion").checked = currentWorking.merge_permanent_exclusion;
    document.getElementById("currentWorkingMergePerm").innerHTML = currentWorking.merge_permanent;
    document.getElementById("currentWorkingMergePermEx").innerHTML = currentWorking.merge_permanent_exclusion;
    document.getElementById("currentWorkingTotal").innerHTML = currentWorking.results.length;
    document.getElementById("currentWorkingSearch").innerHTML = currentWorking.last_search_value || 'n/a';
    document.getElementById("currentWorkingFilter").innerHTML = currentWorking.last_filter || 'n/a';
    $('#collection').val('working_file:current');
    var me = currentWorking.merged_files;
    if (!me){
        me = 'n/a';
    } else {
        if (me.length > 1){
            me = ulFromArray(me);
        }
    }
    document.getElementById("currentWorkingMerged").innerHTML = me;
}

// create ul for array
function ulFromArray(arr){
    var ul1 = '<ul>';
    var ul2  = '';
    arr.forEach(function(d){
        ul2 = ul2 + '<li>'+d+'</li>';
    });
    var ul3 = '</ul>';
    var ul = ul1 + ul2 + ul3;
    return ul;
}

// callback on change the collection from dropdown
$("#collection").change(function(){
	//currentWorking['collection'] = this.options[this.selectedIndex].value ;
        //collectionToQuery = this.options[this.selectedIndex].value ;
        if (confirm('Would you like to delete the current working file and make the selection? Pressing "cancel" selects the collection/file only.')){
            currentWorking = getOriginalWorking();
            $('#working li').removeClass('active');
            Pace.start;
            $('#submit_query').click();
            $('#working #current').parent().addClass('active');
            $(this).blur();
        }
    });


// handle the submission of query 
$("#submit_query").click(function(){
    var val = document.getElementById("query").value;
    excluded = currentWorking['excluded'];
    outside = currentWorking['outside'];
    $(".query_results tbody tr").each(function(i) {
        currentWorking.previous_spot = 1;
    });
    query(val, excluded, outside, null);
    $('#submit_query').blur()
});

// handle submission of excel query
$("#submit_excel_query").click(function(){
    var val = 'excel';
    excluded = currentWorking['excluded'];
    outside = currentWorking['outside'];
    $(".query_results tbody tr").each(function(i) {
        currentWorking.previous_spot = 1;
    });
    query(val, excluded, outside, null);
    $('#submit_excel_query').blur()
});

// handle save working file
$(".submit_save_working").click(function(){
    refresh();
    $('.submit_save_working').blur()
});

// handle adding a record to permanent collecion
$("#submit_add_record").click(function(){
    var val = document.getElementById("add_record").value;
    add(val);
    $('#submit_add_record').blur()
});

// handle exclude record from working file
$("#submit_exclude_record").click(function(){
    var val = document.getElementById("exclude_record").value.split(',');
    val.forEach(function(d){
       d = parseInt($.trim(d).toString());
       if ( !(currentWorking['excluded'].indexOf(d) > -1)){
          currentWorking['excluded'].push(d);	
      }
  });
    $('#submit_exclude_record').blur()
    document.getElementById("exclude_record").value = "";
    setCurrent();
});

// include an outside record into the working file
$("#submit_outside_record").click(function(){
    var val = document.getElementById("outside_record").value.split(',');
    val.forEach(function(d){
       d = parseInt($.trim(d).toString());
       if ( !(currentWorking['outside'].indexOf(d) > -1)){
          currentWorking['outside'].push(d);	
      }
  });
    query(currentWorking['query'], currentWorking['excluded'], currentWorking['outside'], currentWorking['collection']);
    $('#submit_outside_record').blur()
    document.getElementById("outside_record").value = "";
});

// undo excluding a record from this working file
$("#submit_undo_exclude_record").click(function(){
    var val = document.getElementById("undo_exclude_record").value.split(',');
    val.forEach(function(d){
       d = parseInt($.trim(d));
       var index = currentWorking['excluded'].indexOf(d);
       if ( index > -1){
          currentWorking['excluded'].splice(index, 1);	
      }
  });
    $('#submit_undo_exclude_record').blur()
    document.getElementById("undo_exclude_record").value = "";
    setCurrent();
});


// handles when different key strokes that have special meanings
// like undo, back, forward, etc
$(document).keyup(function(e) {
    var foc = false;
    $(document).find(':input').each(function(){
        if ($(this).is(":focus")){
            foc = true;
        }
    })

    if (!foc){
        if(e.which == 98) {
            $(".previous").click();
        }
        if (e.which == 27){
            if ($('#myModal').is(':visible')){
                $('#myModal').modal('hide');
            } else {
                $('#myModal').modal('show');
                
            }
        }
        if(e.which == 13) {
            key = true;
            $("#DataTables_Table_0_next").click();
            $("#DataTables_Table_1_next").click();
        }

        if (e.which == 82 || e.which == 85){
            var killed = currentWorking.excluded.pop(); 
            setCurrent();
        }

        if(e.which == 80) { 
            $(".query_results tbody tr").each(function(i) {
                toAdd = parseInt($(this).find("td:first").text());
            });
            addPerm(toAdd);
        }

        if(e.which == 69) { 
            $(".query_results tbody tr").each(function(i) {
                toAdd = parseInt($(this).find("td:first").text());
            });
            addPermExclusion(toAdd);
        }

        if(e.which == 92 || e.which == 220 || e.which == 191) {
            var kill_id;
            var td = table.column(0, {search: 'applied', order:'applied', filter: 'applied'}).data();
            var kill = td[table.page()];
            var killSpot = td.indexOf(kill);
            var beforeKillSpot = td[killSpot -1];
            currentWorking.previous_spot = beforeKillSpot;
            if (!(currentWorking.excluded.indexOf(kill) > -1)){
               currentWorking.excluded.push(kill);	
           }
           table.row('#' +kill.toString()).remove().draw( 'page' );
           table2.row('#' + kill.toString()).remove().draw( 'page' );
           factorExcludes();
           showCurrents(); 
           var ccc = 0;
            //setCurrent();
        }
    }
    getSpot();

});

// moves the table to previous spot
function prevSpot(old){
    old = old || currentWorking.previous_spot;
    table.page.jumpToData( old, 0 );
    table2.page.jumpToData( old, 0 );
}

// get a working file when it's clicked
$(document).on("click", "#working a", function(){
	var id = this.id;
	$('#working li').removeClass('active');
  $(this).parent().addClass('active');
  $(this).blur();
  getWorking(id);
  saveState({'last_viewed': id})

});

// add a record to the permanent collection
function addPerm(id){
    if (id){
       $.ajax({
        url: "/api/permanent",
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({'formula_numbers': id.toString(), 'action': 'add'}), 
        complete: function(d){
            setCurrent();
            alert(id + ' added to permanent collection!');
        },
    });
   }
}

// add a record to the exclusion collection 
function addPermExclusion(id){
    if (id){
       $.ajax({
        url: "/api/permanent_exclusion",
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({'formula_numbers': id.toString(), 'action': 'add'}), 
        complete: function(d){
            setCurrent();
            alert(id + ' added to permanent exclusion collection!');
        },
    });
   }
}

// handle merging the permanent collection with the current working
document.getElementById('merge_permanent').onclick = function() {
    if ( this.checked ) {
        mergePermanent();
        setCurrent();
    } else {
        unMergePermanent();
        setCurrent();
    }
};        

// handle merging the exclusion collection with the current working
document.getElementById('merge_permanent_exclusion').onclick = function() {
    if ( this.checked ) {
        mergePermanentExclusion();
        setCurrent();
    } else {
        unMergePermanentExclusion();
        setCurrent();
    }
};        

// delete a working file
$("#delete-working-file").click(function(){
    var data = {'name': currentWorking.name};
    if (confirm('Are you sure you want to delete ' + currentWorking.name + '?')){
       $.ajax({
        url: "/api/working",
        type: 'DELETE',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        complete: function(d){
            if (data.name == 'current'){
                window.location.href = '/';
            } else {
                working();
            }
        },
    });
   }
   $("#delete-working-file").blur();

});

// handle the search of the records in the working file results 
$('.search-text').on( 'keyup', function () {
    var term = '\\b' + this.value + '\\b'; 
    table.column(0).search( term, true, false).draw();
    table2.column(0).search( term, true, false).draw();
    $('.search-text').val(this.value);
    currentWorking.last_search = term;
    currentWorking.last_search_value = this.value;
    prevSpot();
} );

// find which spot we're at  -- might not be necessary any more
function getSpot(){
    var s;
    $(".query_results tbody tr").each(function(i) {
        s = parseInt($(this).find("td:first").text());
        if (!isNaN(s)){
            currentWorking.previous_spot = s; 
            currentWorking.previous_index = table.column(0, {search: 'applied', order:'applied', filter: 'applied'}).data().indexOf(s);
        }
        return false; 
    });
    return s;
}

// handle when we change category for filtering the working files
$('.cat-filter').on( 'change', function () {
    var term = this.value;
    if (term == 0){
        term = "1|2|3|4";
    }
    table.column(3).search( term, true, false).draw();
    table2.column(3).search( term, true, false).draw();
    $('.cat-filter').val(this.value);
    currentWorking.last_filter = term;
    currentWorking.last_filter_value = this.value;
    prevSpot();
    table.page(1).draw();
    table2.page(1).draw();
    $('.cat-filter').blur();
    
} );

// add a colection back to the main working file
$('#add-back').click(function(){
    mergeWithFile();
    $('#add-back').blur();
});

// merge this collection with another file
function mergeWithFile(){
    currentWorking.original_working = getOriginalWorking();
    $.ajax({
     url: "/api/working/merge",
     type: 'POST',
     dataType: 'json',
     contentType: 'application/json; charset=utf-8',
     data: JSON.stringify(currentWorking),
     success: function(d){
        alert(currentWorking.name + ' merged with main file! You may need to refresh the page with your browser to see the results.');
        working();
    },
});
}

// save the current, helper function
function refresh() {
    getSpot();
    if (readyToSave){
        console.log('about to save');
        var wfn = document.getElementById('working_file_name').value;
        if (wfn){
            save_working(true, false, wfn);
        } else{
            save_working(true, false);
        }
    } else {
        console.log('not ready to save so doing nothing!');
    }
};

// find seconds between two times
function secs(t1, t2){
    var dif = t1.getTime() - t2.getTime();
    var Seconds_from_T1_to_T2 = dif / 1000;
    return Math.round(Math.abs(Seconds_from_T1_to_T2));
}

// reset our current -- bassically delete our page and start over
$('#reset-current').click(function(){
    var data = {'name': 'current'};
    if (confirm('Are you sure you want to delete the current working file?')){
        $.ajax({
         url: "/api/working",
         type: 'DELETE',
         dataType: 'json',
         contentType: 'application/json; charset=utf-8',
         data: JSON.stringify(data),
         success: function(d){
            currentWorking = getOriginalWorking();
            setCurrent();
        },
    });
    }
    $("#reset-current").blur();
});

////////////////////////////////////
// all of this is for the idle timer --

var _minutes = 5;
$( document ).idleTimer(1000 * 60 * _minutes);


// make the monitory go to sleep and but auto save before!
$( document ).on( "idle.idleTimer", function(event, elem, obj){
        // function you want to fire when the user goes idle
        $("#sleep").modal('show');
        refresh();
    });


$( document ).on( "active.idleTimer", function(event, elem, obj, triggerevent){
        // function you want to fire when the user becomes active again
        //$('body').dimmer('hide');
    });
////////////////////////////////////


// this is for synchonrization between the two tables
// what to do on clicks of next in the tables
$(document).on('mousedown', '#DataTables_Table_1_next', function (event){
    $("#DataTables_Table_0_next").click();
});

// what to do on clicks of next in the tables
$(document).on('mousedown', '#DataTables_Table_0_next', function (event){
    $("#DataTables_Table_1_next").click();
});

// save if we've been clicking alot
var ecount = 0;
$(document).on("keydown",function(e){
    if (!$('input').is(':focus')){
        ecount ++;
    }
    if (ecount > 40){
        refresh();
        ecount = 0;
    }
});

// sync tables
$(document).on('mousedown', '#DataTables_Table_0_wrapper .paginate_button', function(event){
    var n = parseInt($(this).text());
    if (!isNaN(n)){
        table2.page(n - 1).draw( 'page' );
    }
});

// sync tables
$(document).on('mousedown', '#DataTables_Table_1_wrapper .paginate_button', function(event){
    var n = parseInt($(this).text());
    if (!isNaN(n)){
        table.page(n - 1).draw( 'page' );
    }
});

// sync tables
$(document).on('mousedown', '#DataTables_Table_1_previous', function (event){
    $("#DataTables_Table_0_previous").click();
});

// sync tables
$(document).on('mousedown', '#DataTables_Table_0_previous', function (event){
    $("#DataTables_Table_1_previous").click();
});

// return from sleep
$(document).on('click', '#resume', function (event){
        //$('body').dimmer('hide');
        $("#sleep").modal('hide');
        window.location.href ='/';
    });


// merge permanent collection result set with the current working 
function mergePermanent(){
    var currentFormulas = currentWorking.results.map(function(x){return x.formula_number});
    var p = getPerm();
    p.forEach(function(x){
        x.permanent = true;
        var fn = x.formula_number;
        if (!(currentFormulas.indexOf(fn) > -1)){
            currentWorking.results.push(x); 
        }
    });
    currentWorking.merge_permanent = true;
    getSpot();
}

// undo a merge with the permanent collection
function unMergePermanent(){
    var p = getPerm();
    var pf = p.map(function(x){return x.formula_number});
    currentWorking.results = currentWorking.results.filter(function(x){
        var t = !(pf.indexOf(x.formula_number) > -1)
        return t;
    });
    currentWorking.merge_permanent = false;
}

/// merge the  exclusion with the current worling 
function mergePermanentExclusion(){
    var px = getPermEx();
    var p = getPerm();
    var pfx = px.map(function(d){return d.formula_number});
    var pf = p.map(function(d){return d.formula_number});
    var orig = currentWorking.original_results.map(function(x){return x.formula_number});
    currentWorking.results = currentWorking.results.filter(function(x){
        var isPermEx = pfx.indexOf(x.formula_number) > -1;
        return !isPermEx;
    });
    currentWorking.merge_permanent_exclusion = true;
    getSpot();
}

// undo merge with exlcusion
function unMergePermanentExclusion(){
    var px = getPermEx();
    var o = currentWorking.original_results;
    var of = o.map(function(d){return d.formula_number});
    var cf = currentWorking.results.map(function(d){return d.formula_number});
    px.forEach(function(d){
        var fn = d.formula_number;
        if (!(cf.indexOf(fn) > -1) && of.indexOf(fn) > -1){
            currentWorking.results.push(d);
        }
    });
    currentWorking.merge_permanent_exclusion = false;
}


// make sure we remove excluded records (ones that we removed manually -- this isn't the exclusion collection)
function factorExcludes(){
    currentWorking.results = currentWorking.original_results.filter(function(d){
        return !(currentWorking.excluded.indexOf(d.formula_number) > -1); 
    });
}

// save or sate on the server
function saveState(data){
	$.ajax({
        url: "/api/state",
        type: 'post',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(data),
        complete: function(d){
        },
    });

}
// handle the collection shortcuts (these are helpers for repeated actions)
$(document).on("click", "#collectionShortcuts button", function(){
    if (confirm('Are you sure you want to delete the current working file?')){
        currentWorking = getOriginalWorking();
        $('#working li').removeClass('active');
        Pace.start;
        $('#collection').val(this.id);
        $('#submit_query').click();
        $('#working #current').parent().addClass('active');
        $(this).blur();
    }
});


// submit an export csv
$('#submit_export_csv').click(function(e){
    e.preventDefault();

  var data = $('#export_csv').serializeArray().reduce(function(obj, item) {
      obj[item.name] = item.value;
      return obj;
  }, {});
    
   var records = table.column(0, {search: 'applied', order:'applied', filter: 'applied'}).data().toArray();
   console.log("Length of records being exported: " + records.length);
    data.records = records;
    var url = '/api/export/working_file';
  $.ajax({
    url: url,
    type: 'POST',
    data: JSON.stringify(data), 
    dataType: 'json',
        contentType: 'application/json; charset=utf-8',
    success: function(d){
            console.log(d);
            alert(d.message);
    },
  });
    $('#submit_save_database').blur();
});



$(".copyMe").keyup(function(){
  $(".copyMe").val($(this).val());
});

/// call everything on initial load of the page
working();
collections();
getQueryable();


