# -------------------------------------------------------------------------------
# Name:  AurumService.py
# Purpose:
#   Provivide a Windows service to run Aurum application
#
# Author:   Radomirs Cirskis
#
# Created:  2016-06-15
# Licence:  WTFPL
# -------------------------------------------------------------------------------

from __future__ import print_function

import subprocess
from multiprocessing import Process, current_process, freeze_support
import os
import sys
import time
import traceback
import win32api
import win32con
import win32evtlogutil
import win32evtlog
import win32gui_struct
import webbrowser
from win32com.client import GetObject
import socket


try:
    import winxpgui as win32gui
except ImportError:
    import win32gui

from app import app

if getattr(sys, 'frozen', False):
    dir_ = os.path.dirname(sys.executable)
else:
    dir_ = os.path.dirname(os.path.realpath(__file__))

NAME = 'Aurum'
DISPLAY_NAME = 'Aurum Formula'
ICON = os.path.join(dir_, "goldbars.ico")
if not os.path.exists(ICON):
    ICON = os.path.join(dir_, "icon", "goldbars.ico")

class MessageLevel:
    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
    NOTSET = 0

    _levelNames = {
        CRITICAL: 'CRITICAL',
        ERROR: 'ERROR',
        WARNING: 'WARNING',
        INFO: 'INFO',
        DEBUG: 'DEBUG',
        NOTSET: 'NOTSET',
        'CRITICAL': CRITICAL,
        'ERROR': ERROR,
        'WARN': WARNING,
        'WARNING': WARNING,
        'INFO': INFO,
        'DEBUG': DEBUG,
        'NOTSET': NOTSET,
    }

    typemap = {
        DEBUG: win32evtlog.EVENTLOG_INFORMATION_TYPE,
        INFO: win32evtlog.EVENTLOG_INFORMATION_TYPE,
        WARNING: win32evtlog.EVENTLOG_WARNING_TYPE,
        ERROR: win32evtlog.EVENTLOG_ERROR_TYPE,
        CRITICAL: win32evtlog.EVENTLOG_ERROR_TYPE,
    }


def log_message(message,
                level=MessageLevel.ERROR):
    """
    Log a message into Event Log
    """

    details = traceback.format_exc(sys.exc_info())
    lines = (message,) if details is None else (message, details,)
    win32evtlogutil.ReportEvent(
        NAME, 4, 0,
        MessageLevel.typemap[level],
        lines)


def log_info(message):
    log_message(
        message,
        level=MessageLevel.INFO)


def log_error(message):
    """
    Log an error message into Event Log
    """
    log_message(
        message,
        level=MessageLevel.ERROR)


class SysTrayIcon(object):

    QUIT = 'QUIT'
    SPECIAL_ACTIONS = [QUIT]

    FIRST_ID = 1023

    def __init__(self,
                 icon,
                 hover_text,
                 menu_options,
                 on_quit=None,
                 default_menu_index=None,
                 window_class_name=None,):

        self.icon = icon
        self.hover_text = hover_text
        self.on_quit = on_quit

        menu_options = menu_options + (('Quit', None, self.QUIT),)
        self._next_action_id = self.FIRST_ID
        self.menu_actions_by_id = set()
        self.menu_options = self._add_ids_to_menu_options(list(menu_options))
        self.menu_actions_by_id = dict(self.menu_actions_by_id)
        del self._next_action_id

        self.default_menu_index = (default_menu_index or 0)
        self.window_class_name = window_class_name or "SysTrayIconPy"

        message_map = {win32gui.RegisterWindowMessage("TaskbarCreated"): self.restart,
                       win32con.WM_DESTROY: self.destroy,
                       win32con.WM_COMMAND: self.command,
                       win32con.WM_USER + 20: self.notify, }
        # Register the Window class.
        window_class = win32gui.WNDCLASS()
        hinst = window_class.hInstance = win32gui.GetModuleHandle(None)
        window_class.lpszClassName = self.window_class_name
        window_class.style = win32con.CS_VREDRAW | win32con.CS_HREDRAW
        window_class.hCursor = win32gui.LoadCursor(0, win32con.IDC_ARROW)
        window_class.hbrBackground = win32con.COLOR_WINDOW
        window_class.lpfnWndProc = message_map  # could also specify a wndproc.
        classAtom = win32gui.RegisterClass(window_class)
        # Create the Window.
        style = win32con.WS_OVERLAPPED | win32con.WS_SYSMENU
        self.hwnd = win32gui.CreateWindow(classAtom,
                                          self.window_class_name,
                                          style,
                                          0,
                                          0,
                                          win32con.CW_USEDEFAULT,
                                          win32con.CW_USEDEFAULT,
                                          0,
                                          0,
                                          hinst,
                                          None)
        win32gui.UpdateWindow(self.hwnd)
        self.notify_id = None
        self.refresh_icon()

        win32gui.PumpMessages()

    def _add_ids_to_menu_options(self, menu_options):
        result = []
        for menu_option in menu_options:
            option_text, option_icon, option_action = menu_option
            if callable(option_action) or option_action in self.SPECIAL_ACTIONS:
                self.menu_actions_by_id.add(
                    (self._next_action_id, option_action))
                result.append(menu_option + (self._next_action_id,))
            elif non_string_iterable(option_action):
                result.append((option_text,
                               option_icon,
                               self._add_ids_to_menu_options(option_action),
                               self._next_action_id))
            else:
                log_error("Unknown item: "
                          "option_text=%r, option_icon=%r, option_action=%r" %
                          (option_text, option_icon, option_action))
            self._next_action_id += 1
        return result

    def refresh_icon(self):
        # Try and find a custom icon
        hinst = win32gui.GetModuleHandle(None)
        if os.path.isfile(self.icon):
            icon_flags = win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE
            hicon = win32gui.LoadImage(hinst,
                                       self.icon,
                                       win32con.IMAGE_ICON,
                                       0,
                                       0,
                                       icon_flags)
        else:
            log_error("Can't find icon file - using default.")
            hicon = win32gui.LoadIcon(0, win32con.IDI_APPLICATION)

        if self.notify_id:
            message = win32gui.NIM_MODIFY
        else:
            message = win32gui.NIM_ADD
        self.notify_id = (self.hwnd,
                          0,
                          win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_TIP,
                          win32con.WM_USER + 20,
                          hicon,
                          self.hover_text)
        win32gui.Shell_NotifyIcon(message, self.notify_id)

    def restart(self, hwnd, msg, wparam, lparam):
        self.refresh_icon()

    def destroy(self, hwnd, msg, wparam, lparam):
        if self.on_quit:
            self.on_quit()
        nid = (self.hwnd, 0)
        win32gui.Shell_NotifyIcon(win32gui.NIM_DELETE, nid)
        win32gui.PostQuitMessage(0)  # Terminate the app.

    def notify(self, hwnd, msg, wparam, lparam):
        if lparam == win32con.WM_LBUTTONDBLCLK:
            self.execute_menu_option(self.default_menu_index + self.FIRST_ID)
        elif lparam == win32con.WM_RBUTTONUP:
            self.show_menu()
        elif lparam == win32con.WM_LBUTTONUP:
            pass
        return True

    def show_menu(self):
        menu = win32gui.CreatePopupMenu()
        self.create_menu(menu, self.menu_options)
        # win32gui.SetMenuDefaultItem(menu, 1000, 0)

        pos = win32gui.GetCursorPos()
        # See
        # http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winui/menus_0hdi.asp
        win32gui.SetForegroundWindow(self.hwnd)
        win32gui.TrackPopupMenu(menu,
                                win32con.TPM_LEFTALIGN,
                                pos[0],
                                pos[1],
                                0,
                                self.hwnd,
                                None)
        win32gui.PostMessage(self.hwnd, win32con.WM_NULL, 0, 0)

    def create_menu(self, menu, menu_options):
        for option_text, option_icon, option_action, option_id in menu_options[::-1]:
            if option_icon:
                option_icon = self.prep_menu_icon(option_icon)

            if option_id in self.menu_actions_by_id:
                item, extras = win32gui_struct.PackMENUITEMINFO(text=option_text,
                                                                hbmpItem=option_icon,
                                                                wID=option_id)
                win32gui.InsertMenuItem(menu, 0, 1, item)
            else:
                submenu = win32gui.CreatePopupMenu()
                self.create_menu(submenu, option_action)
                item, extras = win32gui_struct.PackMENUITEMINFO(text=option_text,
                                                                hbmpItem=option_icon,
                                                                hSubMenu=submenu)
                win32gui.InsertMenuItem(menu, 0, 1, item)

    def prep_menu_icon(self, icon):
        # First load the icon.
        ico_x = win32api.GetSystemMetrics(win32con.SM_CXSMICON)
        ico_y = win32api.GetSystemMetrics(win32con.SM_CYSMICON)
        hicon = win32gui.LoadImage(
            0, icon, win32con.IMAGE_ICON, ico_x, ico_y, win32con.LR_LOADFROMFILE)

        hdcBitmap = win32gui.CreateCompatibleDC(0)
        hdcScreen = win32gui.GetDC(0)
        hbm = win32gui.CreateCompatibleBitmap(hdcScreen, ico_x, ico_y)
        hbmOld = win32gui.SelectObject(hdcBitmap, hbm)
        # Fill the background.
        brush = win32gui.GetSysColorBrush(win32con.COLOR_MENU)
        win32gui.FillRect(hdcBitmap, (0, 0, 16, 16), brush)
        # unclear if brush needs to be feed.  Best clue I can find is:
        # "GetSysColorBrush returns a cached brush instead of allocating a new
        # one." - implies no DeleteObject
        # draw the icon
        win32gui.DrawIconEx(hdcBitmap, 0, 0, hicon, ico_x,
                            ico_y, 0, 0, win32con.DI_NORMAL)
        win32gui.SelectObject(hdcBitmap, hbmOld)
        win32gui.DeleteDC(hdcBitmap)

        return hbm

    def command(self, hwnd, msg, wparam, lparam):
        id = win32gui.LOWORD(wparam)
        self.execute_menu_option(id)

    def execute_menu_option(self, id):
        menu_action = self.menu_actions_by_id[id]
        if menu_action == self.QUIT:
            win32gui.DestroyWindow(self.hwnd)
        else:
            menu_action()


def app_run():
    app.run(threaded=True)

def wait_for_listener(port, timeout=10):
    """
    waits for a local listener on PORT
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    time.sleep(1)
    for _ in range(timeout):
        try:
            s.connect(('127.0.0.1', port))
            s.close()
            return True
        except:
            time.sleep(1)
    return False
    
    
class AurumSysTrayIcon(SysTrayIcon):

    def start_app(self):
    
        if self.app_process:
            self.stop_app()
            
        self.app_process = Process(
            target=app_run,
            name="Aurum App")
        self.app_process.start()
        
        if wait_for_listener(5000):
            webbrowser.open("http://localhost:5000/")

            
    def stop_app(self):
        if self.app_process:
            try:
                self.app_process.terminate()
            except Exception as ex:
                log_error("Error occured while stopping the service %r" % NAME)
            finally:
                self.app_process = None

    def restart_app(self):
        if self.app_process:
            self.stop_app()
        self.start_app()
        win32api.MessageBox(
            self.hwnd, "%r service restarted" % NAME,
            DISPLAY_NAME, win32con.MB_OK)

    def quit(self):
        self.stop_app()

    def __init__(self, *args, **kwargs):

        # Flask application process:
        self.app_process = None

        # Register Event log source:
        # win32evtlogutil.AddSourceToRegistry(
        #     NAME, eventLogType='Application')

        self.start_app()

        super(AurumSysTrayIcon, self).__init__(
            ICON,
            hover_text=DISPLAY_NAME,
            menu_options=(('Restart %r' % NAME, ICON, self.restart_app),),
            on_quit=self.quit,
            default_menu_index=1)


def non_string_iterable(obj):
    try:
        iter(obj)
    except TypeError:
        return False
    else:
        return not isinstance(obj, basestring)


if __name__ == '__main__':
    freeze_support()

    count = GetObject('winmgmts:').ExecQuery(
            "SELECT * FROM Win32_Process "
            "WHERE Name = 'aurummanager.exe'").count
    
    if count > 1:
        win32api.MessageBox(
            0, "%r is already running... (%d)" % (NAME, count), 
            DISPLAY_NAME, win32con.MB_OK)
        sys.exit(0)

    AurumSysTrayIcon()
