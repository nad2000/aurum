SetCompress auto
SetCompressor /SOLID lzma
RequestExecutionLevel admin

!include /NONFATAL config.nsh
;;!include x64.nsh

; HM NIS Edit Wizard helper defines
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\${PRODUCT_SHORT_NAME}.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

; MUI 1.67 compatible ------
;;XPStyle on
!include "MUI.nsh"

; MUI Settings
!define MUI_ABORTWARNING
;;!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_ICON "icon\goldbars.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; MUI end ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "Install\setup_${PRODUCT_VERSION}.exe"
InstallDir "C:\${PRODUCT_SHORT_NAME}"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" InstallLocation
ShowInstDetails show
ShowUnInstDetails show
!define NSIS_CONFIG_LOG "yes"

Section "${PRODUCT_NAME}" APPLICATION
  SetOutPath "$INSTDIR"
  SetOverwrite try
  File /r "dist\*"
SectionEnd

Section "MongoDB" MONGODB
  SetOutPath "$INSTDIR"
  SetOverwrite try
  File /r /x README.txt /x *.pdb /x mongo.exe /x bsondump.exe /x mongodump.exe /x mongoexport.exe /x mongofiles.exe /x mongoimport.exe /x mongooplog.exe /x mongoperf.exe /x mongorestore.exe /x mongos.exe /x mongostat.exe /x mongotop.exe "mongodb"
  CreateDirectory $INSTDIR\mongodb\db
  CreateDirectory $INSTDIR\mongodb\log
SectionEnd



;;Section -AdditionalIcons
  ;;WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  ;;CreateShortCut "$SMPROGRAMS\{PRODUCT_NAME}\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
  ;;CreateShortCut "$SMPROGRAMS\{PRODUCT_NAME}\Uninstall.lnk" "$INSTDIR\uninst.exe"
;;SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\uninst.exe"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\${PRODUCT_SHORT_NAME}.exe"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "Path" "$INSTDIR\"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\${PRODUCT_SHORT_NAME}.exe"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
  ; Install/reinstall service:
  ;;ExecWait '"$INSTDIR\${PRODUCT_SHORT_NAME}.exe" -remove'
  ;;ExecWait '"$INSTDIR\${PRODUCT_SHORT_NAME}.exe" -install'
  
  ;create desktop shortcut
  CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\${MAIN_EXE}" "" "$INSTDIR\goldbars.ico"
 
  ;create start-menu items
  CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME}"
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk" "$INSTDIR\${MAIN_EXE}" "" "$INSTDIR\goldbars.ico" 0  
  
  ; Seting up MongoDB service:
  CreateDirectory $INSTDIR\mongodb\db
  CreateDirectory $INSTDIR\mongodb\log
  ExecWait 'netsh advfirewall firewall add rule name="Open Port Mongo" dir=in action=allow protocol=TCP localport=27017'
  ExecWait 'netsh advfirewall firewall add rule name="Open Port Mongo" dir=in action=allow protocol=TCP localport=38128'
  ExecWait 'NET STOP AurumMongoDB'
  ExecWait '"$INSTDIR\mongodb\bin\mongod.exe" --serviceName AurumMongoDB --remove'
  ExecWait '"$INSTDIR\mongodb\bin\mongod.exe" --serviceName AurumMongoDB --install --port 38128 --logappend --serviceDisplayName "Aurum MongoDB" --serviceDescription "Aurum MongoDB (Lintening on port 38128)" --dbpath "$INSTDIR\mongodb\db" --logpath "$INSTDIR\mongodb\log\database.log" --journal --storageEngine=mmapv1'
  ExecWait 'NET START AurumMongoDB'
  
SectionEnd


Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
FunctionEnd

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
  Abort
FunctionEnd

Section Uninstall
  ExecWait 'NET STOP AurumMongoDB'
  ExecWait '"$INSTDIR\mongodb\bin\mongod.exe" --serviceName AurumMongoDB --remove'

  Delete "$INSTDIR\${PRODUCT_NAME}.url"
  Delete "$INSTDIR\*.*"
  
  Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk"
  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}"

  RMDir /r /REBOOTOK "$INSTDIR\*"
  
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\Website.lnk"
  Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk"

  RMDir "$SMPROGRAMS\${PRODUCT_NAME}"
  RMDir "$INSTDIR"

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
  SetAutoClose true
SectionEnd
