# -*- coding: utf-8 -*-
from __future__ import print_function

__author__ = 'Radomirs Cirskis (nad2000@gmail.com)'

NAME = "Aurum"
VERSION = "3.3.1"
ASSEMBLY_VERSION = "%s.1" % VERSION
DESCRIPTION = "Aurum Formula App"
WEBSITE = "http://www...."
COMPANY_NAME = "Normann (%s)" % WEBSITE
PUBLISHER = WEBSITE
REVISION =  "%s %s" % (NAME, VERSION)
AUTHOR = __author__
COPYRIGHT = "(c) 2016 Normann"
LEGAL_COPYRIGHT = "%s %s" % (COMPANY_NAME, COPYRIGHT)
LEGAL_TRADEMARK = LEGAL_COPYRIGHT
EMAIL = "___@______"
MAIN_EXE = "aurummanager.exe"

import sys
from glob import glob
from distutils.core import setup
import py2exe

# NSIS configuration:
with open("config.nsh", "w") as nsh:
    print("""
!define PRODUCT_NAME "%(description)s"
!define PRODUCT_VERSION "%(version)s"
!define PRODUCT_PUBLISHER "%(publisher)s"
!define PRODUCT_WEB_SITE "%(website)s"
!define PRODUCT_SHORT_NAME "%(name)s"
!define MAIN_EXE "%(main_exe)s"
VIAddVersionKey "ProductName" "%(description)s"
VIAddVersionKey "Comments" "%(description)s"
VIAddVersionKey "CompanyName" "%(company_name)s"
VIAddVersionKey "LegalTrademarks" "%(legal_trademark)s"
VIAddVersionKey "LegalCopyright" "%(legal_copyright)s"
VIAddVersionKey "FileDescription" "%(description)s Setup"
VIAddVersionKey "FileVersion" "%(version)s"  
VIFileVersion "%(assembly_version)s"
VIProductVersion "%(assembly_version)s"
    """ % dict(
        name=NAME,
        description=DESCRIPTION,
        version=VERSION,
        publisher=PUBLISHER,
        company_name=COMPANY_NAME,
        legal_trademark=LEGAL_TRADEMARK,
        legal_copyright=LEGAL_COPYRIGHT, 
        website=WEBSITE,
        main_exe=MAIN_EXE,
        assembly_version=ASSEMBLY_VERSION,), file=nsh)

        
MANIFEST_TEMPLATE = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
  <assemblyIdentity
    version="%(version)s"
    processorArchitecture="*"
    name="%(prog)s"
    type="win32"
  />
  <description>%(description)s</description>
  <trustInfo xmlns="urn:schemas-microsoft-com:asm.v3">
    <security>
      <requestedPrivileges>
        <requestedExecutionLevel
            level="%(level)s"
            uiAccess="false">
        </requestedExecutionLevel>
      </requestedPrivileges>
    </security>
  </trustInfo>
  <dependency>
    <dependentAssembly>
        <assemblyIdentity
            type="win32"
            name="Microsoft.VC90.CRT"
            version="9.0.30729.4918"
            processorArchitecture="X86"
            publicKeyToken="1fc8b3b9a1e18e3b"
            language="*"
        />
    </dependentAssembly>
    <!-- dependentAssembly>
        <assemblyIdentity
            type="win32"
            name="Microsoft.Windows.Common-Controls"
            version="6.0.0.0"
            processorArchitecture="*"
            publicKeyToken="6595b64144ccf1df"
            language="*"
        />
    </dependentAssembly -->
  </dependency>
</assembly>
"""

RT_BITMAP = 2
RT_MANIFEST = 24


def manifesest(prog_name):
    return (
        RT_MANIFEST, 1,
        (MANIFEST_TEMPLATE %
            dict(
                prog=prog_name,
                version=ASSEMBLY_VERSION,
                description=DESCRIPTION,
                level="asInvoker")).encode("utf-8")
    )

# ['static', 'templates', 'files', 'debug']
data_files = [(d, glob("%s/*" % d)) for d in [
    "templates", "files", "debug", "static/css", 
    "static/fonts", "static/images", "static/js"]]
data_files.append(("", [r"icon/goldbars.ico"]))

setup(
    name=NAME,
    description=DESCRIPTION,
    version=VERSION,
    author=AUTHOR,
    author_email=EMAIL,
    maintainer=AUTHOR,
    maintainer_email=EMAIL,
    windows=[dict(
        script="aurummanager.py",
        company_name=COMPANY_NAME,
        copyright=COPYRIGHT,
        legal_copyright=LEGAL_COPYRIGHT,
        legal_trademark=LEGAL_TRADEMARK,
        product_version=REVISION,
        product_name=DESCRIPTION,
        icon_resources=[
            ( 0, "icon/goldbars.ico"), 
            ( 1, "icon/goldbars.ico"), 
            (42, "icon/goldbars.ico")],
        other_resources=[
            manifesest(NAME),
            # for bitmap resources, the first 14 bytes must be skipped when reading the file:
            (RT_BITMAP, 1, open("icon/goldbars48.bmp", "rb").read()[14:]),
            ("VERSIONTAG", 1, VERSION)
        ]
    )],
    options={'py2exe': {
        #"bundle_files": 1,
        "compressed": True,
        #"xref": True,
        "optimize": "2",
        "dll_excludes": [
            "msvcr71.dll", 
            "msvcp90.dll", 
            #"numpy-atlas.dll",
            #"libiomp5md.dll"
        ],
        'packages': [
            'jinja2.ext', 'jinja2', 'flask',
            'pymongo', 'pyparsing', 'pytz', 'xlrd',
        ],
        'includes': [
            'os', 'logging', 'flask', "win32com", "win32api",
            "win32evtlogutil", "win32evtlog",],
        "excludes": [
            "tkinter",
            "tcl",
            "pywin.dialogs.list",
            "pywin.dialogs",
            "pywin.debugger.dbgcon",
            "pywin.debugger",
            "pywin",
            "pyreadline",
            #"pickle",
            "optparse",
            #"locale",
            "doctest",
            #"difflib",
            #"calendar"
            "_ssl",
            "Tkinter",
            "Tkconstants",
            "PyQt5",
        ],
    }},
    data_files=data_files
)