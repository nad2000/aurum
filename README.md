Getting started
===============

#### The main application is a flask app and the front-end is mostly js/jquery. Any developer familiar with Flask (or Django)
and Javascript will have no trouble editing the application. Knowing MongoDB is helpfup but not required.

Structure
===============

#### app.py is the entry point, main.py does most of the heavy lifting using evaluate.py.

#### excel.py / excel_path.txt are for excel queries

#### templates/ is where the front end design sits

#### static/js is where the js files are that communicate with app.py -- generally a .html page in the application has a corresponding .js file with a similar or same name.

#### files/ and debug/ are legacy and not really needed

Packaging
===============

tools/, mongodb/, icon/, install.nsi, make.cmd, make_inst.cmd, py2exe_setup.py, and requirements.txt are used for packaging and not used while the application runs

## Dependencies

 - Python2.7 (**NB!** 32bit version)
 - NSIS 3.0 (http://nsis.sourceforge.net/Download)
 - Py2exe: py2exe-0.6.9.win32-py2.7.exe (downloaded from: https://sourceforge.net/projects/py2exe/files/py2exe/0.6.9/)
 - Pywin32: pywin32-220.win32-py2.7.exe (https://sourceforge.net/projects/pywin32/files/pywin32/Build%20220/)
 - Python packages: flask, pymongo, pyparsing, pytz, xlrd, virtualenv
 - MongoDB (server): in this directory __mongodb__ save __MongoDB__ installation .ZIP file content downloaded from https://www.mongodb.org/dl/win32/i386, eg, http://downloads.mongodb.org/win32/mongodb-win32-i386-3.2.7.zip (to make build process faster, remove *.pdb, mongo.exe, bsondump.exe, mongodump.exe, mongoexport.exe, mongofiles.exe, mongoimport.exe, mongooplog.exe, mongoperf.exe, mongorestore.exe, mongos.exe, mongostat.exe, and mongotop.exe)

## Build Environment Setup

1. Install **NSIS**
2. Change the current directory to the project directory **aurum**
3. Compress __MongoDB__ executables: `Files\tools\upx391w\upx.exe --best Files\mongodb\bin\*.exe`
3. Install **virtualenv** package: `py -2 -m pip install -U virtualenv`
3. Create virtual environemt: `virtualenv.exe vent` OR `py -2 -m virtualenv venv` (the EXE should be in **c:\Python27\Scripts\**)
4. Activate the virtual environment: `venv\Scripts\activate.bat`
5. Add the packages form *PYPI*: `pip.exe install -U -r Files\requirements.txt`
6. Add **py2exe** and **pywin32** with **easy_install**:
```
  easy_install py2exe-0.6.9.win32-py2.7.exe
  easy_install pywin32-220.win32-py2.7.exe
```

## Building the Application
 
1. Activate the virtual environment: `venv\Scripts\activate.bat`
2. Change the current directory to the project directory **aurum\Files**
3. Adjust options in **py2exe_setup.py**
4. Run **make.cmd** (if the system drive isn't **C:**, you need to modify the scritp **make.cmd**)

## Building the Installation Package

1. Change the current directory to the project directory **aurum\Files**
2. Run **make_inst.cmd** (if the system drive isn't **C:**, you need to modify the scritp **make_inst.cmd**)

Notes
===============

#### the most used section of the front-end is working.html and working.js, this is where
